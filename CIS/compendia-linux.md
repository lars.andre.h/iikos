[[_TOC_]]

## About this Compendium

This is NOT A TEXTBOOK. This a just a collection of lecture notes
together with weekly exercises and labs.

Special thanks go out to the following individuals who provided valuable
feedback: Mariusz Nowostawski, Christopher Frantz, Ivar Farup, Ernst
Gunnar Gran, Eigil Obrestad, Donn Morrison and Slobodan Petrovic.

# Login, Files and Directories

## What Is the Shell (TLCL chp 1)

  - Shell

  - Terminal

  - Prompt

  - date, cal, df, free

  - History  
    arrow up and down, Also TAB-completion and CTRL-r

## Navigation (TLCL chp 2)

  - Root directory

  - Current working directory

  - Parent directory

  - Home directory

  - pwd, ls, cd

  - Absolute vs Relative path

  - cd .

  - cd ..

  - cd -

  - cd \(\sim\)  
    (same as `cd` without any arguments, but note that \(\sim\) is
    useful when giving path names relative to your home directory)

  - ls -a

## Exploring the System (TLCL chp 3)

  - Command Option(s) Argument(s)

  - ls -ltr \(\sim\)

  - file, less

  - Configuration file

  - Script

  - File System: home, root, etc, bin, tmp, media

  - Symbolic link

## Manipulating Files and Directories (TLCL chp 4)

  - Wildcards: \* ?

  - cp, mkdir, mv, rm, ln -s  
    (you can skip the topic “Hard links”)

  - The editor vi: insert/command mode

## About the Lab Exercises

Note: text in `UPPER_CASE` should be replaced by you when you type the
example commands.

### If not a student at NTNU

![image](img/net-setup-fig-nonNTNU.png)

### If you are a student at NTNU

![image](img/net-setup-fig.png)

## Review questions and problems

1.  What is the relationship between a Shell and a Terminal?
    
    1.  A Terminal and a Shell is the same thing.
    
    2.  A Terminal interprets commands and a Shell is a program we use
        to get access to the Terminal.
    
    3.  A Terminal is what we use to terminate a Shell.
    
    4.  A Shell interprets commands and a Terminal is a program we use
        to get access to the Shell.

2.  Which one of the following examples of file paths is an absolute
    pathname?
    
    1.  `./work/courses`
    
    2.  `work/courses`
    
    3.  `/home/ubuntu/work/courses`
    
    4.  `../ubuntu/work/courses`

3.  How do you create a symbolic link from `/var/www/html` to your
    current working directory?
    
    1.  `ln -s /var/www/html .`
    
    2.  `ln /var/www/html .`
    
    3.  `ln /var/www/html /.`
    
    4.  `ln -s /var/www/html ~`

4.  Copy the file `/etc/passwd` to your home directory. Rename it to
    `mypasswd`

5.  Open the file `mypasswd` with `vi`. Delete the first five lines of
    the file, save and quit.

6.  Create a directory `backups`. Copy all files from another directory
    to the `backups` directory by copying recursively and preserving all
    time stamps.

## Lab tutorials

1.  **Log in to a publically available NTNU Linux server**.
    
    Right click on the start button, click Windows PowerShell. When in
    PowerShell (or in a terminal on Mac or Linux): `ssh
    YOUR_USERNAME@login.stud.ntnu.no`
    
    Note: when you have ssh, you also have scp for copying files, just
    remember to add `:` at the end of the command, e.g. if I create a
    local file  
    `echo yo > mysil.txt`  
    I want to copy to the server:  
    `scp mysil.txt YOUR_USERNAME@login.stud.ntnu.no:`  
    and if I want to copy that file from the server and back to my local
    host:  
    `scp YOUR_USERNAME@login.stud.ntnu.no:~/mysil.txt .`

2.  **Log in to your own SkyHiGh Linux server**.
    
    Follow the [instructions for creating an infrastructure in
    SkyHiGh](https://gitlab.com/erikhje/dcsg1005/blob/master/heat-labs.md)
    and use the template called [single linux
    20.04](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_linux_20.04.yaml).
    
    See [the instructions at the
    end](https://gitlab.com/erikhje/dcsg1005/blob/master/heat-labs.md#accessing-linux-instances)
    which explains that you have to do ssh and scp slightly differently
    now. Repeat the previous ssh/scp exercise using your own SkyHiGh
    Linux server instead of `login.stud.ntnu.no`.

3.  **Commands and command line history**.
    
    1.  Try all the commands in chapter one of .
    
    2.  Use the up and down arrows to see how you can repeat commands
        from your history.
    
    3.  Do  
        `cat .bash_history`  
        log out and log back in again, and repeat  
        `cat .bash_history`
    
    4.  Google to try and find out the default maximum number of
        commands saved into the `.bash_history` file.

4.  **Navigation**.
    
    1.  Try all the commands in chapter two of .

5.  **Exploring the system.**.
    
    1.  Try all the commands in chapter three of . You do not have to
        know the entire file system hierarchy in table 3.4 but know that
        you can look it up when needed. *Also try to use TAB completion
        to quickly do the following commands*  
        
            cd /usr/local/share/ca-certificates/
            ls -l /var/lib/ubuntu-release-upgrader/release-upgrade-available
            cd /usr/share/perl-openssl-defaults/
            cd /usr/include/openmpi/pmix/src/include
    
    2.  Make sure you understand how to search in a text file that you
        are viewing with `less` (`/mysil` (the ’`/`’ is the "search
        command") will search for the text `mysil` and `n` will show you
        the next occurrence). This is useful because this is how it
        works in a couple of other widely used text editors and viewers
        as well.
    
    3.  Note: find out how you can copy and paste. This works in
        different ways dependent on which terminal you are using and if
        you are using Linux, Mac or Windows. Ask teacher if unsure.
    
    4.  Do `ls -l` in your home directory. Do you have any symbolic
        links there?

6.  **Manipulating files and directories**.
    
    1.  Try all the commands in chapter four of .
    
    2.  Do the following
        
            cd
            cp /etc/passwd .
            ls -l passwd
            cp -a /etc/passwd .
            ls -l passwd
        
        What does the -a option do to the cp command?
    
    3.  In chapter five of , read carefully (and do the commands) in the
        section about `man`

7.  **(OPTIONAL EXERCISE) Editing a file with vi**.
    
    1.  Read and do all the commands on pages 141-145 (the first five
        pages) in chapter 12 of .

8.  **(OPTIONAL EXERCISE) Keep your session: Screen and tmux**.
    
    `screen` is the old classic “terminal multiplexer” while `tmux` is a
    newer version with some more features. If you know the basics of one
    of these, you can keep your session (everything you are currently
    doing) even if you log out or lose your network connection.
    
    1.  Log in to the server (replace this command if not a
        NTNU-student)  
        `ssh YOUR_USERNAME@login.stud.ntnu.no`
    
    2.  Start a tmux session and run a command in it  
        `tmux`  
        `ls`
    
    3.  Leave the tmux session by typing CTRL-b then d (for detach).
    
    4.  Log out of the server  
        `exit`
    
    5.  Log in to the server (replace this command if not a
        NTNU-student)  
        `ssh YOUR_USERNAME@login.stud.ntnu.no`
    
    6.  Go back into your tmux session  
        `tmux a`
    
    7.  See how you can do other things, e.g. create multiple windows
        and scroll up and down at <https://tmuxcheatsheet.com/>.

# Redirections and Expansions

## Redirection (TLCL chp 6)

  - Two types of output

  - STDIN, STDOUT, STDERR

  - Create, Append, Read  
    `>, >>, <`

  - STDERR to STDOUT  
    `2>&1`

  - /dev/null

  - cat

  - CTRL-d

  - Pipelines

  - Important difference  
    `| vs >`

  - Filters: sort, uniq, wc, grep

  - grep -irv

  - head/tail -n -f

  - tee

## Expansion (TLCL chp 7)

  - Expansion  
    “Each time we type a command and press the Enter key, bash performs
    several substitutions upon the text before it carries out our
    command. We have seen a couple of cases of how a simple character
    sequence, for example `*`, can have a lot of meaning to the shell.
    The process that makes this happen is called expansion” (, page 68).

  - $((2+2))

  - {A,B,C}, {01..12}

  - printenv

  - $( )

  - VARIABLES

  - Double quotes

  - Single quotes

  - Escaping a character

## Review questions and problems

1.  Only one of these makes sense, which one?
    
    1.  `$ command | file`
    
    2.  `$ file > command`
    
    3.  `$ command > file`
    
    4.  `$ file | command`

2.  What is the output of the command `echo "$(2 + 2)"` ?
    
    1.  `4`
    
    2.  `2 + 2`
    
    3.  `$(2 + 2)`
    
    4.  `2: command not found`

3.  Which character to you put in front of `$` if you want `echo "$a"`
    to actually print `$a` (and not expand it to the variable `a`) ?
    
    1.  `\`
    
    2.  `` ` ``
    
    3.  `|`
    
    4.  `/`

4.  Create the following directory structure using `mkdir -p` and brace
    expansion.
    
        .
        |--A
        |  |--0
        |  |--1
        |
        |--B
        |  |--0
        |  |--1
        |
        |--C
           |--0
           |--1
    
    Verify that it has been created with the `ls` command by using the
    option for recursive listing. Delete the entire directory structure
    you created with a single command.

5.  Write a command pipeline to list files in a directory hierarchy
    (e.g. your home directory) that have the name pattern `*.txt` and
    sort them by filesize.

6.  Write a command pipeline to recursively search for the word
    `hostname` in `/etc`. You should redirect standard error to
    `/dev/null` so you don’t see any "Permission denied" messages.

7.  Write a command to show the first five lines of the file
    `~/.bashrc`.

8.  Write a command pipeline to write lines 21-24 of `~/.bashrc` to a
    new file `strange.tmp`.

## Lab tutorials

1.  **Redirection and pipelines.**
    
    1.  Try all the commands in chapter six of .
    
    2.  What can you do with command `grep -ir mysil ~`

2.  **Expansion.**
    
    1.  Try all the commands in chapter seven of . On page 76, remember
        that you can create a file like this  
        `echo Mysil > "two words.txt"`
    
    2.  Do `man bash` and search in this man-page for the headline
        “ARITHMETIC EVALUATION”, and try the following commands
        
            i=5
            ((i++))
            echo $i
    
    3.  What was the `i` ? It was a variable, note that you need to
        avoid spaces when assigning variables otherwise Bash will
        interpret it as a command with options/arguments. Try the
        following commands
        
            a = Mysil
            echo $a
            a=Mysil
            echo $a
            a=$(pwd)
            echo $a

3.  **Extracting a number from a table.**
    
    We can show lines matching patterns with `grep`. What if we want to
    show only the third column from the lines? Try the following
    commands
    
        grep cpu0 /proc/stat
        grep cpu0 /proc/stat | awk '{print $3}'
        num=$(grep cpu0 /proc/stat | awk '{print $3}')
        echo $num
        ((num++))
        echo $num
    
    Note the command `awk '{print $3}'` to show column three.

# Permissions and Processes

## Permissions (TLCL chp 9)

  - Ownership  
    user, group, everyone

  - id

  - rwxrwxrwx

  - chmod

  - Number systems  
    binary (01), octal (0..7), decimal (0..9), hexadecimal (0..9A..F)

  - SetUID

  - sudo -s vs -i  
    (Note: Ubuntu disables logins to the root account)

  - chown, chgrp

  - adduser

## Processes (TLCL chp 10)

  - Multitasking and multiuser

  - Process ID (PID)

  - Process state

  - ps aux, top, htop

  - background  
    `nano &` or `nano`, CTRL-z, `bg`

  - foreground  
    `fg`

  - jobs

  - CTRL-c

  - kill

  - reboot, shutdown

  - pstree  
    (check out `tree` for files as well)

  - PATH  
    When you run a command, if it’s not a Bash builtin (like `cd`), Bash
    will look for the program in the directories listed in the
    environment variable PATH, and execute the first matching program it
    encounters.

## Review questions and problems

1.  Connect the commands in each column to the correct keyword.
    
    |  | `chmod` | `ps` | `ln -s` | `sudo` | `kill` | `id` |
    | :- | :------ | :--- | :------ | :----- | :----- | :--- |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    

2.  Connect the octal number in each column to the correct file
    permissions.
    
    |  | `640` | `777` | `700` | `755` | `600` | `444` |
    | :- | :---- | :---- | :---- | :---- | :---- | :---- |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    

3.  Write a command to show the line containing your username in
    `/etc/passwd` (note: this doesn’t work on *login* since that server
    is configured to use centralized user accounts instead of the local
    ones in `/etc/passwd`). Add a pipeline to the command to show only
    the fifth column where columns are separated with `:` (hint: `man
    cut`).

4.  Write commands for the following
    
    1.  Create a file `a.txt`
    
    2.  Change permissions on `a.txt` to `r--r-----`
    
    3.  Try to delete `a.txt` (redo steps above if you are able to
        delete it)
    
    4.  Change permissions on `a.txt` to `rw-------`
    
    5.  Add a user `mysil`
    
    6.  Change the owner of `a.txt` to be `mysil`
    
    7.  Try to delete `a.txt` (redo steps above if you are able to
        delete it)
    
    8.  Move `a.txt` to `/tmp/`
    
    9.  Try to delete `/tmp/a.txt`

5.  Write a command pipeline to show the three processes owned by you
    that uses the most RAM/physical memory (the RSS column you get from
    the `ps` command).

6.  Start a process in the background. Remove it with the `kill`
    command.

## Lab tutorials

1.  **Permissions.**
    
    1.  Try all the commands in chapter nine of . Some commands (like
        `id`) you should try on both *login* and *VM*. Some of the
        commands for changing ownership and permissions might involve
        several user accounts. On *VM*, you have root access and can
        create new accounts, e.g.  
        `sudo adduser mysil`
    
    2.  See if any of the programs available to you have the SetUID-bit
        set  
        `ls -l /usr/bin | grep rws`

2.  **Processes.**
    
    1.  Try all the commands in chapter ten of . When you log in from
        remote, you will probably not be able to run GUI-programs like
        `xlogo` so replace `xlogo` with `nano` in the examples.
    
    2.  Do `pstree -p | less` Why isn’t `init` PID 1 like the book says?
        Google and/or ask teacher.

3.  **The environment variable PATH.**
    
    1.  Do `echo $PATH` on *VM*, then do
        
            VM$ mkdir ~/bin
            VM$ cat > ~/bin/ls
            #!/bin/bash
            echo "I love seeing files and directories!" 
            /bin/ls
            CTRL-d
            VM$ chmod +x ~/bin/ls
        
        log out and back in again, run `ls`, what happens and why?
    
    *Remember to always test your code. Linux commands and Bash are very
    powerful and it is easy to make mistakes. Make a note of the
    following references for future use:
    [ShellCheck](https://www.shellcheck.net/) and [Bash
    Pitfalls](https://mywiki.wooledge.org/BashPitfalls).*
