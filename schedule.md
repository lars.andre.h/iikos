# About the Course
## Expected background
We will use Linux command line and C-programming in this course, see the [CIS-document](https://gitlab.com/erikhje/iikos/-/blob/master/CIS/README.md) for information about Linux (NTNU-students can also just login (ssh) to `login.stud.ntnu.no` for most exercises).
* BIDATA know programming but not C, have some Linux experience but also have IIKG1001 in parallell where Linux is covered in the first weeks.
* BPROG know C-programming and some Linux from IIKG1001.
* DIGSEC know C-programming and Linux from DCSG1001.
* CIS know some C-programming, some computer architecture and some Linux/Python from Raspberry PI in the networks course.
## Course setup
| Time&Place | Happening |
| ---- | ------------- |
| Tuesdays 0815-1000 A255/A254 | Lab/Exercises BIDATA,BPROG with Sonja and Håkon |
| Tuesdays 1215-1000 S206 | Lecture ALL with Erik |
| Tuesdays 1415-1600 A255/A254 | Lab/Exercises CIS with Eirik and Håkon |
| Thursdays 0915-1100 A255/A254 | Lab/Exercises DIGSEC with Sonja and Simon |
| Thursdays 1115-1300 Teams | Lab/Exercises BIDATA,BPROG,DIGSEC with Simon and Eirik |
| Thursdays 1215-1300 S206 | Lecture/Spørretime BIDATA,BPROG,DIGSEC with Erik |

([See tp](https://tp.uio.no/ntnu/timeplan/timeplan.php?id=IDATG2202&type=course&sort=form&sem=20h) for official schedule of course).

Every week we have two hours of lectures plus one additional session for CIS at Jørstadmoen and one additional session for NTNU-students. In addition the teaching assistants are available for four hours per week (two hours scheduled in class rooms, two hours online in Teams). All the two-hour lectures are [available in Omnom](https://forelesning.gjovik.ntnu.no/publish/index.php?lecturer=all&topic=IDATG2202+Operativsystemer). In addition there will be video lectures available in the weekly schedule below.

## Mandatory assignments
Approximately every second week there will be an online quiz (see Test #1, Test #2, ... in the schedule below). Each test will have ten questions (multiple choice with four answer options where only one is correct, no negative points meaning you are free to guess the answer if you get stuck). You need six correct (out of ten) to pass the test, and you need to pass three out of five tests in total to qualify for the exam in this course. The questions you get in these tests are in the same format and complexity level that you will get on some parts of the final exam.

## Download course material
Clone the git repos (remember you can update any time with `git pull`):
```sh
git clone https://github.com/remzi-arpacidusseau/ostep-code.git
git clone https://github.com/remzi-arpacidusseau/ostep-homework.git
git clone https://gitlab.com/erikhje/iikos-files.git # demos and example files
git clone https://gitlab.com/erikhje/iikos.git       # compendia and course docs
```
## How to get a good grade
In prioritized order:
1. Do all the Lab exercises and Review questions and problems in [the compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf).
1. Read the compendia and the weekly chapters in the textbook as stated in the weekly schedule below. _When you read in the textbook, pay attention to what is included in the compendia and spend your time reading the parts in the textbook that have focus in the compendia_.
1. Watch all the lectures.
## Weekly schedule
| Week | Topic (Chapters) | Additional Info |
| ---- | -------------    | --------------- |
| 34   | **Computer architecture** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 1, [potensregning](https://gitlab.com/erikhje/iikos/-/blob/master/potensregning.pdf))<br/>ALU, CU, I/O, bus, MMU, controller, firmware, bit vs Byte, data vs instruction, instruction set, micro architecture, register, AX/BX/CX/DX/SP/BP/IP/IR/FLAG/PSW, address, interrupt, interrupt handler/routine, stack/push/pop, context switch, assembly directive/label, clock-speed/frequency/period, Hz, pipeline, micro-operations, out-of-order execution, branch prediction, superscalar, SMT/hyperthreading, CPU/IO-bound processes, von Neumann-bottle neck, spatial/temporal locality, cache line, write through/back cache | Video-lecture: [computer architecture part 3](https://youtu.be/gHlDm6aBBfY) |
| 35   | **Introduction and processes** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 2, textbook chp [2](http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf), [4](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-intro.pdf))<br/>process, process API, thread/multi-threaded, process states (ready/running/blocked), PCB, process list/table, address space, file, design goals, timesharing, batch jobb/process, interactive job/process, GNU, POSIX, bit/Byte, KB/MB/GB/TB/PB/EB, ms/us/ns, gcc | Test #1 |
| 36   | **System calls** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 3, textbook chp [5](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-api.pdf), [6](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-mechanisms.pdf), (see also this: [page 1-8](http://cslibrary.stanford.edu/102/PointersAndMemory.pdf)))<br/>fork, copy-on-write, exec, wait, signal, limited direct execution, instructions/system calls/commands, kernel mode, user mode, mode switch/transition, preemptive multitasking, trap table (interrupt vector table), sync/async interrupts, software/exception/hardware interrupt, timer interrupt, Process ID (PID), (call) stack, kernel stack, privileged operation/instruction, cooperative vs preemptive (timer interrupt) | |
| 37   | **Scheduling** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 4, textbook chp [7](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched.pdf), [8](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-mlfq.pdf), [9.1](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-lottery.pdf), [10.1, 10.3](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-multi.pdf)<br/>clock interrupt, preemptive vs non-preemptive, turnaround time, response time, workload, FCFS/FIFO, convoy effect, SJF, STCF, Round Robin, time quantum, jiffie, MLFQ, priority levels, boost, dynamic priority, fair-share/lottery scheduling, CPU-pinning/affinity, gang-/co-scheduling | Test #2 |
| 38   | **Address spaces and paging** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 5, textbook chp [13](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-intro.pdf), [14](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-api.pdf), [15](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-mechanism.pdf), [16.1, 16.4](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-segmentation.pdf), [17.1](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf), [18](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-paging.pdf))<br/>address space, kernel/user space, multiprogramming, stack/heap memory, malloc(), free(), valgrind, translation, relocation, base and bound/limit registers, segments, free list, bitmap, external/internal fragmentation, paging, offset, page, page frame, virtual/physical address, page table, page table entry (PTE), present/absent bit, referenced bit, modified/dirty bit, memory trace |  |
| 39   | **Memory management** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 6, textbook chp [19](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-tlbs.pdf), [20](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-smalltables.pdf), [21](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-beyondphys.pdf), [22](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-beyondphys-policy.pdf), [23.2](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-complete.pdf))<br/>TLB, hit/miss, hit rate, temporal/spatial locality, TLB entry, ASID, multi-level page table, PTBR, PDBR, CR3, inverted page table, swap, page fault, minor/major page fault, optimal/fifo/random/LRU/clock page replacement, demand-paging vs pre-paging/pre-fetching, thrashing, working set, hugepages | Video-lecture: [memory management part 3](https://youtu.be/g-iudRz0kl4), Test #3 |
| 40   | **Threads and locks** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 7, textbook chp [26](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-intro.pdf), [27](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-api.pdf), [28.1-9, 28.12-13](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks.pdf))<br/>PCB vs TCB, single- vs multi-thread, pthread create/join, atomicity, critical section, race condition, deterministic, mutual exclusion, mutex lock, test-and-set, xchg, compare-and-swap, cmpxchg, lock prefix, spin/busy waiting, spin or switch, yield, two-phase lock |  |
| 41   | **Condition variables, semaphores, concurrency** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 8, textbook chp [30](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-cv.pdf), [31](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-sema.pdf))<br/>pthread cond_wait/cond_signal, producer-consumer, semaphore, sem_wait (down), sem_post (up), binary semaphore, ordering/synchronizing semaphore, reader-writer, starvation, dining philosophers, barrier, monitor, deadlock |  |
| 42   | **I/O and HDD/SSD** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 9, textbook chp [36.1-36.7](http://pages.cs.wisc.edu/~remzi/OSTEP/file-devices.pdf), [37.1-37.4](http://pages.cs.wisc.edu/~remzi/OSTEP/file-disks.pdf), [RAID](https://commons.wikimedia.org/w/index.php?title=Redundant_array_of_independent_disks&oldid=148689461), [44.1-6, 44.10-12](http://pages.cs.wisc.edu/~remzi/OSTEP/file-ssd.pdf))<br/> Memory and I/O buses/interconnect, PCI/USB/SATA, micro-controller, I/O device, programmed I/O, interrupt-based I/O, DMA, I/O instructions (isolated I/O), memory-mapped I/O, I/O stack, block device, storage stack, block addresses, sector, HDD, platter, surface, spindle, RPM, track, cylinder, disk arm, disk head, seek time, rotational delay, SSD, SLC/MLC/TLC, NAND flash, flash translation layer, trim, write amplification, wear levelling, RAID 0/1/5, iops, sequential/random read/write | Test #4 |
| 43   | **File systems** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 10, textbook [39.1-4, 39.7-18](http://pages.cs.wisc.edu/~remzi/OSTEP/file-intro.pdf), [40](http://pages.cs.wisc.edu/~remzi/OSTEP/file-implementation.pdf), [42.1-2](http://pages.cs.wisc.edu/~remzi/OSTEP/file-journaling.pdf))<br/>inode, open(), read(), write(), close(), STDIN/STDOUT/STDERR, file descriptor, fsync, metadata, strace, link/unlink, mkdir(), opendir(), readdir(), closedir(), rmdir(), hard link, symbolic link, permission bits (rwx), SetUID, SetGID, sticky bit, chmod(), chown(), mkfs, mount, inode/data bitmap, metadata, superblock, single/double/triple indirect pointers/addressing, extents, EXT, page cache, sleuthkit, fsck, journalling, idempotent  |  |
| 44   | **Virtual Machines and Containers** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 11, [Hardware Virtualization: the Nuts and Bolts](https://gitlab.com/erikhje/iikos/-/blob/master/anandtech.pdf))<br/>unikernel, sensitive/privileged instructions, trap-and-emulate, binary translation, basic blocks, paravirtualization, hardwaresupported virtualization, vmx/svm/ept/npt/vpid/asid/vt-d, shadow/guest/physical page table, page walk, CR3, ballooning, IOMMU, SR-IOV, virtual function, nested virtualization, cgroup, namespaces, union mounts, Docker | Test #5 |
| 45   | **Operating System Security** ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 12, textbook [53](http://pages.cs.wisc.edu/~remzi/OSTEP/security-intro.pdf), [55](http://pages.cs.wisc.edu/~remzi/OSTEP/security-access.pdf))<br/>security policy, CIA, secure systems design principles, reference monitor, identification, authentication, authorization, capability, ACL, ACE, access token, security descriptor, privileges, MAC, DAC, mandatory integrity control, DACL, SACL, integrity levels, SID, secure attention sequence, UAC, namespace virtualization, UID/GID, sudo, buffer overflow, heap spraying, nop sled, stack canary, DEP, NX, return-to-libc, ASLR | Lab/demo resources: [FC4 VMware VM](https://folk.ntnu.no/erikhje/vmware/FC4.zip) or [FC4 VirtualBox VM](https://folk.ntnu.no/erikhje/vmware/FC4-SISTE.ova) (data / poL+Vot2), [Return-to-libc](http://cse.sustech.edu.cn/faculty/~zhangfw/19fa-cs315/labs/lab12-return-to-libc.pdf), [Stack](https://folk.ntnu.no/erikhje/opsys/callstack.pdf) |
| 46   | Repetition |  |
| 47   | Repetition<br/>exam info |  |

<!---
IF WE TRY TO SQUEEZE IN COMPUTER ARCHITECTURE IT WILL BE TOO TIGHT:

| Week | Topic (Chapters) | Exercises | Additional Resources |
| ---- | -------------    | --------- | -------------------- |
| 34   | Arch, X86 Assembly, Registers | | |
| 35   | CPU-details, Multicore/SMT | | |
| 36   | Cache, I/O, Interrupts | | |
| 37   | Intro, Process (2, 4) |  |  |
| 38   | SysCalls (5, 6)|  |  |
| 39   | Scheduling (7, 8, 9.1, 10.1, 10.3) |  |  |
| 40   | Address spaces/translation, C (13, 14, 15) |  |  |
| 41   | Segmentation and paging (16, 17, 18, 19) |  |  |
| 42   | Memory management (20, 21, 22) |  |  |
| 43   | Threads, locks, conditional variables (26, 27, 28, 29, 30) |  |  |
| 44   | Semaphores, concurrency problems (31, 32) |  |  |
| 45   | I/O, RAID (36, 37.1-37.4, 38)  |  |  |
| 46   | File systems, FSCK/journalling, SSD (39, 40, 42, 44) |  |  |
| 47   | Virtualization and containers (separate document) |  |  |
--->
